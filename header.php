<!doctype html>							
<html dir="ltr" lang="en"> 
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>	

	<title>@TODO NAME</title>
	<meta name="description" content=""/>

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/src/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/assets/src/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/assets/src/favicon/favicon-16x16.png">
	
	<link rel="mask-icon" href="/assets/src/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/assets/src/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-config" content="/assets/src/favicon/browserconfig.xml">

    <link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <!-- <link rel="mask-icon" href="/assets/manifest/safari-pinned-tab.svg" color="#ffffff" /> -->

    <link rel="manifest" href="/assets/src/favicon/site.webmanifest" />
    <meta name="theme-color" content="#ffffff" />
    <meta name="msapplication-config" content="/assets/manifest/browserconfig.xml" />

    <link rel="stylesheet" href="/assets/public/css/frontend.css" /> 
</head>

<body>

