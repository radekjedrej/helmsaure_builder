<section class="info-row__wrapper">
	<div class="info-row">
		<div class="info-row__container wrapper-full">
			<h4 class="info-row__title text-header color-lightblue">Was sind die Leistungen unseres Produkts?</h4>
			<div class="info-row__flex d-flex">
				<div class="info-row__content">
					<div class="info-row__copy text-body f-300">
					<p><strong>Wir analysieren den bestehenden Versicherungsschutz und ermitteln bedarfsgerechte, individuelle Versicherungslösungen. Hier bieten wir ein spezielles Konzept für Verbandsmitglieder, das besonders auf die Bedürfnisse von Arztpraxen ausgelegt ist.</strong></p>

					<p>Neben dem Versicherungsschutz erhalten Sie auch bereits einige der genannten Service-Leistungen und Schulungsprogramme kostenfrei.   Sollte es trotz aller Vorsicht und Schutzmaßnahmen dennoch zu einem Cyberangriff kommen, erhalten Sie schnelle und kompetente Hilfe von uns. Wir lassen Sie nicht im Regen stehen!</p>
					</div>
				</div>
				<a class="info-row__casing d-block" href="#">
					<div class="info-row__box">
						<div class="info-row__box__title color-white text-box f-300">Spezielles Konzept für Verbandsmitglieder:</div>
						<div class="info-row__box__text color-white text-box f-700">Informieren Sie sich jetzt!</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
