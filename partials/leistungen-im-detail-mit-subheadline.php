<section class="benefits-dropdown__wrapper">
	<div class="benefits-dropdown wrapper-small">
		<div class="benefits-dropdown__container">
			<!-- Tab Start -->
			<div class="benefits-dropdown__content d-tab">
				<div class="benefits-dropdown__oblique d-flex d-tab__box">
					<div class="benefits-dropdown__box">
						<div class="benefits-dropdown__headline">
							<h4 class="benefits-dropdown__title text-headline f-300">Software Selekt: Das Kontrollprogramm</h4>
						</div>
						<div class="benefits-dropdown__arrow d-tab__arrow">
							<svg class="benefits-dropdown__svg" width="26" height="41" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="benefits-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="benefits-dropdown__segment">
					<div class="benefits-dropdown__row d-flex d-flex-wrap">
						<div class="benefits-dropdown__column">
							<h5 class="benefits-dropdown__column__title text-body f-600">Privatpatienten</h5>
							<ul class="benefits-dropdown__list list-reset">
								<li class="benefits-dropdown__item text-body f-300">Alleine <strong>über 35 geschulte Abrechnungsberater garantieren eine qualifizierte Beratung</strong> ...</li>
								<li class="benefits-dropdown__item text-body f-300">Unsere leistungsfähige, <strong>auf die individuellen Belange...</strong></li>
								<li class="benefits-dropdown__item text-body f-300">Da wir <strong>innerhalb von fünf Arbeitstagen den Abrechungsbetrag auszahlen</strong>, erhalten Sie ...</li>
								<li class="benefits-dropdown__item text-body f-300">Ihre <strong>Praxisorganisation</strong> wird <strong>durch</strong> die <strong>Auslagerung von Abrechnung</strong> und <strong>Mahnwesen vereinfacht.</strong></li>
							</ul>
						</div>
						<div class="benefits-dropdown__column">
							<h5 class="benefits-dropdown__column__title text-body f-600">Selektivverträge</h5>
							<ul class="benefits-dropdown__list list-reset">
								<li class="benefits-dropdown__item text-body f-300">Eine <strong>kostenfrei</strong> zur Verfügung gestellte <strong>Abrechnungs-software zur lokalen Installation</strong> in der jeweiligen...</li>
								<li class="benefits-dropdown__item text-body f-300">Ein <strong>Online-Portal</strong> mit einer umfassenden <strong>Information</strong> der Arztpraxis <strong>zum Stand der übermittelten...</strong></li>
								<li class="benefits-dropdown__item text-body f-300">Ein grundlegendes <strong>Verständnis</strong> der <strong>ambulanten und stationären Versorgungsprozesse</strong> in <strong>allen Facetten unterschiedlicher Fachspezialitäten.</strong></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Tab End -->
		</div>
	</div>
</section>
