<section class="custom-text__wrapper">
	<div class="custom-text">
		<div class="custom-text__container wrapper-small">
			<div class="custom-text__headline d-flex">
				<div class="custom-text__svg">
					<svg width="69" height="69" viewBox="0 0 69 69" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M34.3985 67.7951C52.7667 67.7951 67.7951 52.9336 67.7951 34.3985C67.7951 15.8634 52.7667 1.00195 34.3985 1.00195C16.0304 1.00195 1.00195 15.8634 1.00195 34.3985C1.00195 52.9336 16.0304 67.7951 34.3985 67.7951Z" fill="white"/><path d="M34.3985 2.50474C51.9317 2.50474 66.2922 16.8653 66.2922 34.3985C66.2922 51.9317 51.9317 66.2922 34.3985 66.2922C16.8653 66.2922 2.50474 51.9317 2.50474 34.3985C2.50474 16.8653 16.8653 2.50474 34.3985 2.50474ZM34.3985 0C15.3624 0 0 15.3624 0 34.3985C0 53.4345 15.3624 68.797 34.3985 68.797C53.4345 68.797 68.797 53.4345 68.797 34.3985C68.797 15.3624 53.2676 0 34.3985 0Z" fill="#00335E"/><path d="M36.9034 50.763V56.2735H31.3929V50.763H36.9034ZM39.4081 48.2583H28.8882V58.7782H39.4081V48.2583Z" fill="#00ADF0"/><path d="M37.2373 15.0287L35.9014 43.0818H32.3948L30.8919 15.0287H37.2373ZM39.909 12.5239H28.2202L29.89 45.5865H38.2392L39.909 12.5239Z" fill="#00ADF0"/></svg>
				</div>
				<div class="custom-text__heading">
					<h4 class="custom-tet__title text-benefits f-600">Unser Tipp</h4>
				</div>
			</div>
			<div class="custom-text__copy text-body f-300">
				<p>Als Minimaldeckung empfehlen wir für Personen- und Sachschäden eine pauschale Deckungssumme von 5.000.000 Euro und 300.000 Euro bei Vermögensschäden.

				<p>Wichtig ist zudem, neben der Berücksichtigung der Themen:</p>

				<ul class="custom-text__list list-reset">
					<li class="custom-text__item">Erweiterter Strafrechtsschutz</li>
					<li class="custom-text__item">Mitversicherung weiterer Praxisstandorte und/oder angestellter Fachärzte</li>
					<li class="custom-text__item">Berücksichtigung der Rechtsform der Praxis (MVZ, BAG, üöBAG,...)</li>
					<li class="custom-text__item">Nachhaftungsversicherung bei Berufsaufgabe</li>
				</ul>

				<p>dass uns jedwede Änderung Ihres Tätigkeitsfeldes mitgeteilt wird. Wenn Sie z.B. eine beleg- oder konsiliarärztliche Tätigkeit aufnehmen, muss die Anzahl der Belegbetten genannt werden usw. <a href="#" class="custom-text__link">// Jetzt Kontakt aufnehmen!</a></p>
			</div>
		</div>
	</div>
</section>
