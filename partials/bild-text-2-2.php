<section class="standard-row__wrapper wrapper-full standard-row--large-margin standard-row--fifty standard-row--header-l-margin">
    <div class="standard-row d-flex">
        <div class="standard-row__image">
            <img class="standard-row__img d-block" src="https://via.placeholder.com/430x200" alt="">
        </div>
        <div class="standard-row__content">
			<!-- if($title): -->
            <h4 class="standard-row__title text-header f-300">Headline</h4>
			<!-- endif; -->

			<!-- if($subtitle): -->
            <h5 class="standard-row__subtitle text-subheader f-600">Branchenspezifische, leistungsstarke Konzepte und Dienstleistungen</h5>
			<!-- endif; -->

            <div class="standard-row__copy text-body f-300">
                <p>Lorem ipsum dae ra sum que <strong>perum cullabo</strong>. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit <strong>asperes suntium facepro</strong> quid modis <strong>vollectis</strong> int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus.</p>
				<p>Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo <strong>optatiorerum</strong> imaio. Bus conseque volute <strong>cuptatem faceris</strong> inveliquo is as dolesti aliquas ernam aborero omnihil lorem. Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. <strong>Derepudi dolupta</strong> tendae nimagnimodit asperes <strong>suntium facepro</strong> quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
            </div>
        </div>
    </div>
</section>
