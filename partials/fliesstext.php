<section class="heavy-content__wrapper">
	<div class="heavy-content wrapper-full">
		<div class="heavy-content__container">
			<h2 class="heavy-content__title text-header f-300">SEO Text Versicherungen allgemein</h2>
			<div class="heavy-content__copy text-body f-300">
				<p>Niedergelassene Ärzte in eigener Praxis haften in Ausübung ihrer Tätigkeit ihren Patienten gegenüber. Und dies nicht nur für selbst verschuldete Schäden, sondern ggf. auch für Fehler von Angestellten oder Praxispartnern. Hierbei können nicht versicherte Ansprüche unter Umständen auch existenzbedrohend sein. Sichern Sie sich daher lieber umfassend ab! <a class="heavy-content__link" href="#">// Jetzt Kontakt aufnehmen!</a></p>
			</div>	
		</div>
	</div>
</section>
