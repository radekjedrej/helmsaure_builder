<section class="card-carousel__wrapper wrapper-full">
	<div class="card-carousel">
		<h4 class="card-carousel__title text-subheader f-600">Empfehlungen</h4>
		<div class="card-carousel__arrow card-carousel__left">
			<svg viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M16.0756 1.88245L2.1543 15.9695L16.0756 30.0566" stroke="#00ADF0" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</div>
		<div class="card-carousel__arrow card-carousel__right">
			<svg viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M1.75879 30.0565L15.8459 15.9694L1.75879 2.04803" stroke="#00ADF0" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</div>
		<div class="card-carousel__container wrapper-small">
			<div class="card-carousel__track swiper">
				<div class="card-carousel__row swiper-wrapper">

					<div class="card-carousel__slide swiper-slide d-flex">
						<div class="card-carousel__segment d-flex">
							<div class="card-carousel__image">
								<svg class="card-carousel__svg" viewBox="0 0 64 55" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M34.8861 10.7222H8.28223V12.0936H34.8861V10.7222Z" fill="#00ADF0"/><path d="M30.243 22.3169H8.28223V23.6883H30.243V22.3169Z" fill="#00ADF0"/><path d="M32.1254 16.457H8.28223V17.8285H32.1254V16.457Z" fill="#00ADF0"/><path d="M27.3567 28.1768H8.28223V29.5482H27.3567V28.1768Z" fill="#00ADF0"/><path d="M27.3567 33.9116H8.28223V35.283H27.3567V33.9116Z" fill="#00ADF0"/><path d="M57.0489 6.20941L42.7627 20.4028L43.7388 21.3726L58.025 7.17914L57.0489 6.20941Z" fill="#00335E"/><path d="M62.3689 4.48831L59.7336 1.87013C57.9767 0.124671 55.2159 0.124671 53.459 1.87013L35.263 19.948C35.263 19.948 35.1375 20.1974 35.1375 20.3221L31.7492 31.6675C31.6238 31.9169 31.7492 32.1662 31.8747 32.2909C32.0002 32.4156 32.1257 32.5403 32.3767 32.5403C32.5022 32.5403 32.5022 32.5403 32.6277 32.5403L43.9218 29.0493C44.0473 29.0493 44.2983 28.9247 44.2983 28.9247L62.3689 10.7221C64.1257 8.97662 64.1257 6.23376 62.3689 4.48831ZM33.3806 30.7948L36.1414 21.6935L42.4159 28.0519L33.3806 30.7948ZM61.4904 9.72467L43.7963 27.3039L36.7689 20.3221L54.463 2.74285C55.7179 1.4961 57.6002 1.4961 58.8551 2.74285L61.4904 5.36104C62.6198 6.60779 62.6198 8.60259 61.4904 9.72467Z" fill="#00335E"/><path d="M48.6902 29.6725V35.7816C48.6902 39.5219 45.6784 42.5141 41.9137 42.5141H23.9686L22.5882 43.8855L13.9294 52.1141V43.7609V42.6388H8.40784C4.64313 42.6388 1.63137 39.6466 1.63137 35.9063V11.2206C1.63137 7.48034 4.64313 4.48813 8.40784 4.48813H41.9137V3.1167H8.40784C3.89019 3.1167 0.250977 6.85696 0.250977 11.2206V35.7816C0.250977 40.2699 3.89019 43.8855 8.40784 43.8855H12.549V53.6102C12.549 53.8596 12.6745 54.1089 12.9255 54.2336C13.051 54.2336 13.051 54.2336 13.1765 54.2336C13.302 54.2336 13.5529 54.1089 13.6784 54.1089L24.4706 43.8855H41.9137C46.4314 43.8855 50.0706 40.2699 50.0706 35.7816V29.6725H48.6902Z" fill="#00335E"/></svg>
							</div>
							<div class="card-carousel__content">
								<p class="card-carousel__copy text-body-sm f-300">Die Helmsauer Gruppe ist ein starker Partner in allen Versicherungs- und Finanzbelangen. Wir sind dort mit mehreren Aufträgen angesiedelt und sind in jedem einzelnen Aspekt sehr zufrieden. Besser können wir es uns nicht vorstellen. Weiter so!</p>
								<div class="card-carousel__signature text-body-sm-2 f-600">Max Mustermann von der Charite, Berlin</div>
							</div>
						</div>
						<div class="card-carousel__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.78 984.93"><polyline points="2.39 984.19 307.39 2.5 2.39 2.5" style="fill:none;stroke:#e5f1fa;stroke-miterlimit:10;stroke-width:5px"/></svg>
						</div>
					</div>

					<div class="card-carousel__slide swiper-slide d-flex">
						<div class="card-carousel__segment d-flex">
							<div class="card-carousel__image">
								<svg class="card-carousel__svg" viewBox="0 0 64 55" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M34.8861 10.7222H8.28223V12.0936H34.8861V10.7222Z" fill="#00ADF0"/><path d="M30.243 22.3169H8.28223V23.6883H30.243V22.3169Z" fill="#00ADF0"/><path d="M32.1254 16.457H8.28223V17.8285H32.1254V16.457Z" fill="#00ADF0"/><path d="M27.3567 28.1768H8.28223V29.5482H27.3567V28.1768Z" fill="#00ADF0"/><path d="M27.3567 33.9116H8.28223V35.283H27.3567V33.9116Z" fill="#00ADF0"/><path d="M57.0489 6.20941L42.7627 20.4028L43.7388 21.3726L58.025 7.17914L57.0489 6.20941Z" fill="#00335E"/><path d="M62.3689 4.48831L59.7336 1.87013C57.9767 0.124671 55.2159 0.124671 53.459 1.87013L35.263 19.948C35.263 19.948 35.1375 20.1974 35.1375 20.3221L31.7492 31.6675C31.6238 31.9169 31.7492 32.1662 31.8747 32.2909C32.0002 32.4156 32.1257 32.5403 32.3767 32.5403C32.5022 32.5403 32.5022 32.5403 32.6277 32.5403L43.9218 29.0493C44.0473 29.0493 44.2983 28.9247 44.2983 28.9247L62.3689 10.7221C64.1257 8.97662 64.1257 6.23376 62.3689 4.48831ZM33.3806 30.7948L36.1414 21.6935L42.4159 28.0519L33.3806 30.7948ZM61.4904 9.72467L43.7963 27.3039L36.7689 20.3221L54.463 2.74285C55.7179 1.4961 57.6002 1.4961 58.8551 2.74285L61.4904 5.36104C62.6198 6.60779 62.6198 8.60259 61.4904 9.72467Z" fill="#00335E"/><path d="M48.6902 29.6725V35.7816C48.6902 39.5219 45.6784 42.5141 41.9137 42.5141H23.9686L22.5882 43.8855L13.9294 52.1141V43.7609V42.6388H8.40784C4.64313 42.6388 1.63137 39.6466 1.63137 35.9063V11.2206C1.63137 7.48034 4.64313 4.48813 8.40784 4.48813H41.9137V3.1167H8.40784C3.89019 3.1167 0.250977 6.85696 0.250977 11.2206V35.7816C0.250977 40.2699 3.89019 43.8855 8.40784 43.8855H12.549V53.6102C12.549 53.8596 12.6745 54.1089 12.9255 54.2336C13.051 54.2336 13.051 54.2336 13.1765 54.2336C13.302 54.2336 13.5529 54.1089 13.6784 54.1089L24.4706 43.8855H41.9137C46.4314 43.8855 50.0706 40.2699 50.0706 35.7816V29.6725H48.6902Z" fill="#00335E"/></svg>
							</div>
							<div class="card-carousel__content">
								<p class="card-carousel__copy text-body-sm f-300">Die Helmsauer Gruppe ist ein starker Partner in allen Versicherungs- und Finanzbelangen. Wir sind dort mit mehreren Aufträgen angesiedelt und sind in jedem einzelnen Aspekt sehr zufrieden. Besser können wir es uns nicht vorstellen. Weiter so!</p>
								<div class="card-carousel__signature text-body-sm-2 f-600">Max Mustermann von der Charite, Berlin</div>
							</div>		
						</div>
						<div class="card-carousel__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.78 984.93"><polyline points="2.39 984.19 307.39 2.5 2.39 2.5" style="fill:none;stroke:#e5f1fa;stroke-miterlimit:10;stroke-width:5px"/></svg>
						</div>
					</div>

					<div class="card-carousel__slide swiper-slide d-flex">
						<div class="card-carousel__segment d-flex">
							<div class="card-carousel__image">
								<svg class="card-carousel__svg" viewBox="0 0 64 55" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M34.8861 10.7222H8.28223V12.0936H34.8861V10.7222Z" fill="#00ADF0"/><path d="M30.243 22.3169H8.28223V23.6883H30.243V22.3169Z" fill="#00ADF0"/><path d="M32.1254 16.457H8.28223V17.8285H32.1254V16.457Z" fill="#00ADF0"/><path d="M27.3567 28.1768H8.28223V29.5482H27.3567V28.1768Z" fill="#00ADF0"/><path d="M27.3567 33.9116H8.28223V35.283H27.3567V33.9116Z" fill="#00ADF0"/><path d="M57.0489 6.20941L42.7627 20.4028L43.7388 21.3726L58.025 7.17914L57.0489 6.20941Z" fill="#00335E"/><path d="M62.3689 4.48831L59.7336 1.87013C57.9767 0.124671 55.2159 0.124671 53.459 1.87013L35.263 19.948C35.263 19.948 35.1375 20.1974 35.1375 20.3221L31.7492 31.6675C31.6238 31.9169 31.7492 32.1662 31.8747 32.2909C32.0002 32.4156 32.1257 32.5403 32.3767 32.5403C32.5022 32.5403 32.5022 32.5403 32.6277 32.5403L43.9218 29.0493C44.0473 29.0493 44.2983 28.9247 44.2983 28.9247L62.3689 10.7221C64.1257 8.97662 64.1257 6.23376 62.3689 4.48831ZM33.3806 30.7948L36.1414 21.6935L42.4159 28.0519L33.3806 30.7948ZM61.4904 9.72467L43.7963 27.3039L36.7689 20.3221L54.463 2.74285C55.7179 1.4961 57.6002 1.4961 58.8551 2.74285L61.4904 5.36104C62.6198 6.60779 62.6198 8.60259 61.4904 9.72467Z" fill="#00335E"/><path d="M48.6902 29.6725V35.7816C48.6902 39.5219 45.6784 42.5141 41.9137 42.5141H23.9686L22.5882 43.8855L13.9294 52.1141V43.7609V42.6388H8.40784C4.64313 42.6388 1.63137 39.6466 1.63137 35.9063V11.2206C1.63137 7.48034 4.64313 4.48813 8.40784 4.48813H41.9137V3.1167H8.40784C3.89019 3.1167 0.250977 6.85696 0.250977 11.2206V35.7816C0.250977 40.2699 3.89019 43.8855 8.40784 43.8855H12.549V53.6102C12.549 53.8596 12.6745 54.1089 12.9255 54.2336C13.051 54.2336 13.051 54.2336 13.1765 54.2336C13.302 54.2336 13.5529 54.1089 13.6784 54.1089L24.4706 43.8855H41.9137C46.4314 43.8855 50.0706 40.2699 50.0706 35.7816V29.6725H48.6902Z" fill="#00335E"/></svg>
							</div>
							<div class="card-carousel__content">
								<p class="card-carousel__copy text-body-sm f-300">Die Helmsauer Gruppe ist ein starker Partner in allen Versicherungs- und Finanzbelangen. Wir sind dort mit mehreren Aufträgen angesiedelt und sind in jedem einzelnen Aspekt sehr zufrieden. Besser können wir es uns nicht vorstellen. Weiter so!</p>
								<div class="card-carousel__signature text-body-sm-2 f-600">Max Mustermann von der Charite, Berlin</div>
							</div>
						</div>
						<div class="card-carousel__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.78 984.93"><polyline points="2.39 984.19 307.39 2.5 2.39 2.5" style="fill:none;stroke:#e5f1fa;stroke-miterlimit:10;stroke-width:5px"/></svg>
						</div>
					</div>

					<div class="card-carousel__slide swiper-slide d-flex">
						<div class="card-carousel__segment d-flex">
							<div class="card-carousel__image">
								<svg class="card-carousel__svg" viewBox="0 0 64 55" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M34.8861 10.7222H8.28223V12.0936H34.8861V10.7222Z" fill="#00ADF0"/><path d="M30.243 22.3169H8.28223V23.6883H30.243V22.3169Z" fill="#00ADF0"/><path d="M32.1254 16.457H8.28223V17.8285H32.1254V16.457Z" fill="#00ADF0"/><path d="M27.3567 28.1768H8.28223V29.5482H27.3567V28.1768Z" fill="#00ADF0"/><path d="M27.3567 33.9116H8.28223V35.283H27.3567V33.9116Z" fill="#00ADF0"/><path d="M57.0489 6.20941L42.7627 20.4028L43.7388 21.3726L58.025 7.17914L57.0489 6.20941Z" fill="#00335E"/><path d="M62.3689 4.48831L59.7336 1.87013C57.9767 0.124671 55.2159 0.124671 53.459 1.87013L35.263 19.948C35.263 19.948 35.1375 20.1974 35.1375 20.3221L31.7492 31.6675C31.6238 31.9169 31.7492 32.1662 31.8747 32.2909C32.0002 32.4156 32.1257 32.5403 32.3767 32.5403C32.5022 32.5403 32.5022 32.5403 32.6277 32.5403L43.9218 29.0493C44.0473 29.0493 44.2983 28.9247 44.2983 28.9247L62.3689 10.7221C64.1257 8.97662 64.1257 6.23376 62.3689 4.48831ZM33.3806 30.7948L36.1414 21.6935L42.4159 28.0519L33.3806 30.7948ZM61.4904 9.72467L43.7963 27.3039L36.7689 20.3221L54.463 2.74285C55.7179 1.4961 57.6002 1.4961 58.8551 2.74285L61.4904 5.36104C62.6198 6.60779 62.6198 8.60259 61.4904 9.72467Z" fill="#00335E"/><path d="M48.6902 29.6725V35.7816C48.6902 39.5219 45.6784 42.5141 41.9137 42.5141H23.9686L22.5882 43.8855L13.9294 52.1141V43.7609V42.6388H8.40784C4.64313 42.6388 1.63137 39.6466 1.63137 35.9063V11.2206C1.63137 7.48034 4.64313 4.48813 8.40784 4.48813H41.9137V3.1167H8.40784C3.89019 3.1167 0.250977 6.85696 0.250977 11.2206V35.7816C0.250977 40.2699 3.89019 43.8855 8.40784 43.8855H12.549V53.6102C12.549 53.8596 12.6745 54.1089 12.9255 54.2336C13.051 54.2336 13.051 54.2336 13.1765 54.2336C13.302 54.2336 13.5529 54.1089 13.6784 54.1089L24.4706 43.8855H41.9137C46.4314 43.8855 50.0706 40.2699 50.0706 35.7816V29.6725H48.6902Z" fill="#00335E"/></svg>
							</div>
							<div class="card-carousel__content">
								<p class="card-carousel__copy text-body-sm f-300">Die Helmsauer Gruppe ist ein starker Partner in allen Versicherungs- und Finanzbelangen. Wir sind dort mit mehreren Aufträgen angesiedelt und sind in jedem einzelnen Aspekt sehr zufrieden. Besser können wir es uns nicht vorstellen. Weiter so!</p>
								<div class="card-carousel__signature text-body-sm-2 f-600">Max Mustermann von der Charite, Berlin</div>
							</div>
						</div>
						<div class="card-carousel__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.78 984.93"><polyline points="2.39 984.19 307.39 2.5 2.39 2.5" style="fill:none;stroke:#e5f1fa;stroke-miterlimit:10;stroke-width:5px"/></svg>
						</div>
					</div>
				</div>
				<div class="card-carousel__pagination"></div>
			</div>
		</div>
	</div>
</section>
