<section class="standard-dropdown__wrapper">
	<div class="standard-dropdown wrapper-small">
		<div class="standard-dropdown__container">
			<!-- Tab Start -->
			<div class="standard-dropdown__tab d-tab standard-dropdown--revert">
				<div class="standard-dropdown__oblique d-tab__box d-flex">
					<div class="standard-dropdown__box">
						<div class="standard-dropdown__headline">
							<h4 class="standard-dropdown__box__title text-headline f-300">Beispiel Element Text links Bild rechts 1:1</h4>
						</div>
						<div class="standard-dropdown__arrow d-tab__arrow">
						<svg class="standard-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="standard-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="standard-dropdown__content">
					<div class="standard-dropdown__flex d-flex d-flex-wrap">
						<div class="standard-dropdown__image">
							<img src="https://via.placeholder.com/430x200" alt="" class="standard-dropdown__img">
						</div>
						<div class="standard-dropdown__copy">
							<div class="standard-dropdown__text text-body f-300">
								<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Tab End -->
			<div class="standard-dropdown__tab d-tab">
				<div class="standard-dropdown__oblique d-flex d-tab__box">
					<div class="standard-dropdown__box">
						<div class="standard-dropdown__headline">
							<h4 class="standard-dropdown__box__title text-headline f-300">Beispiel Element Text rechts Bild links 1:1</h4>
						</div>
						<div class="standard-dropdown__arrow d-tab__arrow">
						<svg class="standard-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="standard-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="standard-dropdown__content">
					<div class="standard-dropdown__flex d-flex d-flex-wrap">
						<div class="standard-dropdown__image">
							<img src="https://via.placeholder.com/430x200" alt="" class="standard-dropdown__img">
						</div>
						<div class="standard-dropdown__copy">
							<div class="standard-dropdown__text text-body f-300">
								<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="standard-dropdown__tab d-tab standard-dropdown--image-small">
				<div class="standard-dropdown__oblique d-flex d-tab__box">
					<div class="standard-dropdown__box">
						<div class="standard-dropdown__headline">
							<h4 class="standard-dropdown__box__title text-headline f-300">Beispiel Element Text rechts Bild links 2:1</h4>
						</div>
						<div class="standard-dropdown__arrow d-tab__arrow">
						<svg class="standard-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="standard-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="standard-dropdown__content">
					<div class="standard-dropdown__flex d-flex d-flex-wrap">
						<div class="standard-dropdown__image">
							<img src="https://via.placeholder.com/430x200" alt="" class="standard-dropdown__img">
						</div>
						<div class="standard-dropdown__copy">
							<div class="standard-dropdown__text text-body f-300">
								<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="standard-dropdown__tab d-tab standard-dropdown--image-small standard-dropdown--revert">
				<div class="standard-dropdown__oblique d-flex d-tab__box">
					<div class="standard-dropdown__box">
						<div class="standard-dropdown__headline">
							<h4 class="standard-dropdown__box__title text-headline f-300">Beispiel Element Text links Bild rechts 2:1</h4>
						</div>
						<div class="standard-dropdown__arrow d-tab__arrow">
						<svg class="standard-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="standard-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="standard-dropdown__content">
					<div class="standard-dropdown__flex d-flex d-flex-wrap">
						<div class="standard-dropdown__image">
							<img src="https://via.placeholder.com/430x200" alt="" class="standard-dropdown__img">
						</div>
						<div class="standard-dropdown__copy">
							<div class="standard-dropdown__text text-body f-300">
								<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Three Columns Tab Start -->
			<div class="standard-dropdown__tab d-tab">
				<div class="standard-dropdown__oblique d-flex d-tab__box">
					<div class="standard-dropdown__box">
						<div class="standard-dropdown__headline">
							<h4 class="standard-dropdown__box__title text-headline f-300">Beispiel Element Icon mit Text dreispaltig</h4>
						</div>
						<div class="standard-dropdown__arrow d-tab__arrow">
						<svg class="standard-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="standard-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="standard-dropdown__content standard-dropdown__segment">
					<div class="standard-dropdown__flex standard-dropdown__row d-flex d-flex-wrap">
						<!-- Column Start -->
						<div class="standard-dropdown__column d-flex">
							<div class="standard-dropdown__icon">
								<svg viewBox="0 0 85 92" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M42.4327 91.6892C42.2657 91.6892 42.2657 91.6892 42.0987 91.6892C25.4004 84.6759 12.7097 69.1465 5.69647 46.7708C0.520001 29.9056 0.186035 14.8771 0.186035 14.7102C0.186035 14.3762 0.353018 14.0422 0.853966 13.8752L42.2657 0.850586C42.4327 0.850586 42.5997 0.850586 42.7666 0.850586L84.1784 13.8752C84.5123 14.0422 84.8463 14.3762 84.8463 14.7102C84.8463 14.8771 84.5123 29.9056 79.3359 46.7708C72.3226 69.1465 59.6319 84.6759 42.7666 91.6892C42.7666 91.6892 42.5997 91.6892 42.4327 91.6892ZM2.02285 15.3781C2.18983 21.7234 5.52949 74.323 42.4327 90.0194C79.5029 74.323 82.8425 21.7234 83.0095 15.3781L42.4327 2.6874L2.02285 15.3781Z" fill="#003764"/><path d="M42.4327 81.6698C42.2657 81.6698 42.2657 81.6698 42.0988 81.6698C10.706 68.4782 9.7041 22.391 9.7041 21.89C9.7041 21.556 9.87108 21.2221 10.372 21.0551L42.4327 11.0361C42.5997 11.0361 42.7667 11.0361 42.9337 11.0361L74.9944 21.0551C75.3283 21.2221 75.6623 21.556 75.6623 21.89C75.6623 22.391 74.6604 68.4782 43.1007 81.5029C42.7667 81.5029 42.5997 81.6698 42.4327 81.6698ZM11.3739 22.5579C11.5409 28.0684 14.3796 67.8103 42.4327 79.833C70.6528 67.8103 73.3245 28.0684 73.6585 22.5579L42.4327 12.706L11.3739 22.5579Z" fill="#003764"/><path d="M39.4268 63.1346C38.091 63.1346 36.9221 62.6336 35.9202 61.6317L23.5635 49.108L30.4098 42.2617L38.7589 50.7778L57.9619 25.0625L65.8101 30.9069L43.4344 61.1308C42.5995 62.2997 41.2636 62.9676 39.9278 63.1346C39.7608 63.1346 39.5938 63.1346 39.4268 63.1346ZM26.0682 49.108L37.2561 60.4628C37.924 61.1308 38.7589 61.4647 39.7608 61.4647C40.5957 61.4647 41.4306 60.9638 42.0986 60.1289L63.6393 31.2409L58.6299 27.5672L39.0929 53.4496L30.5768 44.7665L26.0682 49.108Z" fill="#27AAE1"/></svg>
							</div>
							<div class="standard-dropdown__desc">
								<div class="standard-dropdown__text text-body f-300">
									<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
								</div>
							</div>
						</div>
						<!-- Column End -->
						<div class="standard-dropdown__column d-flex">
							<div class="standard-dropdown__icon">
								<svg viewBox="0 0 85 92" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M42.4327 91.6892C42.2657 91.6892 42.2657 91.6892 42.0987 91.6892C25.4004 84.6759 12.7097 69.1465 5.69647 46.7708C0.520001 29.9056 0.186035 14.8771 0.186035 14.7102C0.186035 14.3762 0.353018 14.0422 0.853966 13.8752L42.2657 0.850586C42.4327 0.850586 42.5997 0.850586 42.7666 0.850586L84.1784 13.8752C84.5123 14.0422 84.8463 14.3762 84.8463 14.7102C84.8463 14.8771 84.5123 29.9056 79.3359 46.7708C72.3226 69.1465 59.6319 84.6759 42.7666 91.6892C42.7666 91.6892 42.5997 91.6892 42.4327 91.6892ZM2.02285 15.3781C2.18983 21.7234 5.52949 74.323 42.4327 90.0194C79.5029 74.323 82.8425 21.7234 83.0095 15.3781L42.4327 2.6874L2.02285 15.3781Z" fill="#003764"/><path d="M42.4327 81.6698C42.2657 81.6698 42.2657 81.6698 42.0988 81.6698C10.706 68.4782 9.7041 22.391 9.7041 21.89C9.7041 21.556 9.87108 21.2221 10.372 21.0551L42.4327 11.0361C42.5997 11.0361 42.7667 11.0361 42.9337 11.0361L74.9944 21.0551C75.3283 21.2221 75.6623 21.556 75.6623 21.89C75.6623 22.391 74.6604 68.4782 43.1007 81.5029C42.7667 81.5029 42.5997 81.6698 42.4327 81.6698ZM11.3739 22.5579C11.5409 28.0684 14.3796 67.8103 42.4327 79.833C70.6528 67.8103 73.3245 28.0684 73.6585 22.5579L42.4327 12.706L11.3739 22.5579Z" fill="#003764"/><path d="M39.4268 63.1346C38.091 63.1346 36.9221 62.6336 35.9202 61.6317L23.5635 49.108L30.4098 42.2617L38.7589 50.7778L57.9619 25.0625L65.8101 30.9069L43.4344 61.1308C42.5995 62.2997 41.2636 62.9676 39.9278 63.1346C39.7608 63.1346 39.5938 63.1346 39.4268 63.1346ZM26.0682 49.108L37.2561 60.4628C37.924 61.1308 38.7589 61.4647 39.7608 61.4647C40.5957 61.4647 41.4306 60.9638 42.0986 60.1289L63.6393 31.2409L58.6299 27.5672L39.0929 53.4496L30.5768 44.7665L26.0682 49.108Z" fill="#27AAE1"/></svg>
							</div>
							<div class="standard-dropdown__desc">
								<div class="standard-dropdown__text text-body f-300">
									<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
								</div>
							</div>
						</div>
						<div class="standard-dropdown__column d-flex">
							<div class="standard-dropdown__icon">
								<svg viewBox="0 0 85 92" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M42.4327 91.6892C42.2657 91.6892 42.2657 91.6892 42.0987 91.6892C25.4004 84.6759 12.7097 69.1465 5.69647 46.7708C0.520001 29.9056 0.186035 14.8771 0.186035 14.7102C0.186035 14.3762 0.353018 14.0422 0.853966 13.8752L42.2657 0.850586C42.4327 0.850586 42.5997 0.850586 42.7666 0.850586L84.1784 13.8752C84.5123 14.0422 84.8463 14.3762 84.8463 14.7102C84.8463 14.8771 84.5123 29.9056 79.3359 46.7708C72.3226 69.1465 59.6319 84.6759 42.7666 91.6892C42.7666 91.6892 42.5997 91.6892 42.4327 91.6892ZM2.02285 15.3781C2.18983 21.7234 5.52949 74.323 42.4327 90.0194C79.5029 74.323 82.8425 21.7234 83.0095 15.3781L42.4327 2.6874L2.02285 15.3781Z" fill="#003764"/><path d="M42.4327 81.6698C42.2657 81.6698 42.2657 81.6698 42.0988 81.6698C10.706 68.4782 9.7041 22.391 9.7041 21.89C9.7041 21.556 9.87108 21.2221 10.372 21.0551L42.4327 11.0361C42.5997 11.0361 42.7667 11.0361 42.9337 11.0361L74.9944 21.0551C75.3283 21.2221 75.6623 21.556 75.6623 21.89C75.6623 22.391 74.6604 68.4782 43.1007 81.5029C42.7667 81.5029 42.5997 81.6698 42.4327 81.6698ZM11.3739 22.5579C11.5409 28.0684 14.3796 67.8103 42.4327 79.833C70.6528 67.8103 73.3245 28.0684 73.6585 22.5579L42.4327 12.706L11.3739 22.5579Z" fill="#003764"/><path d="M39.4268 63.1346C38.091 63.1346 36.9221 62.6336 35.9202 61.6317L23.5635 49.108L30.4098 42.2617L38.7589 50.7778L57.9619 25.0625L65.8101 30.9069L43.4344 61.1308C42.5995 62.2997 41.2636 62.9676 39.9278 63.1346C39.7608 63.1346 39.5938 63.1346 39.4268 63.1346ZM26.0682 49.108L37.2561 60.4628C37.924 61.1308 38.7589 61.4647 39.7608 61.4647C40.5957 61.4647 41.4306 60.9638 42.0986 60.1289L63.6393 31.2409L58.6299 27.5672L39.0929 53.4496L30.5768 44.7665L26.0682 49.108Z" fill="#27AAE1"/></svg>
							</div>
							<div class="standard-dropdown__desc">
								<div class="standard-dropdown__text text-body f-300">
									<p>Lorem ipsum dae ra sum que perum cullabo. Exped quidi nate elisimus sum est, quia nos ipsanturio. Derepudi dolupta tendae nimagnimodit asperes suntium facepro quid modis vollectis int vellique rem fuga. Et ea dolorit atibus quos doluptatur, que et voluptium ad eum fugite peruptisiti ne nostrume ilit, officium harum et mos sa nihilibus. Quo corrum quodi dolupta tenihite venis expla volor aut lab ipsa nem res et voluptatiat mo erorpore con pra voles mo optatiorerum imaio. Bus conseque volute cuptatem faceris inveliquo is as dolesti aliquas ernam aborero omnihil lorem.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Three Columns Tab End -->

		</div>
	</div>
</section>

