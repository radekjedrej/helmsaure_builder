<section class="double-benefits__wrapper">
	<div class="double-benefits">
		<div class="double-benefits__container wrapper-small">
			<div class="double-benefits__tab">
				<div class="double-benefits__oblique d-flex">
					<div class="double-benefits__box">
						<h4 class="double-benefits__title text-headline f-300">Leistungen im Detail</h4>
					</div>
					<div class="double-benefits__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
			</div>
			<div class="double-benefits__flex d-flex d-flex-wrap">
				<!-- Column Start -->
				<div class="double-benefits__column">
					<!-- List Start -->
					<ul class="double-benefits__list list-reset">
						<!-- List Item Start -->
						<li class="double-benefits__item text-body f-300"><strong>Garantierte und unverzügliche Hilfestellung</strong> im Schadenfall – Rund um die Uhr!</li>
						<!-- List Item End -->
						<li class="double-benefits__item text-body f-300"><strong>Zweifache Maximierung</strong> der Versicherungssumme</li>
						<li class="double-benefits__item text-body f-300"><strong>Mitversicherung von Eigenschäden</strong> in der Forensik und Schadenfeststellung</li>
						<li class="double-benefits__item text-body f-300"><strong>Vermögensschäden</strong> aus gefälschten E-Mails mit Aufforderung zu Geldtransaktionen („Fake-President“)</li>
						<li class="double-benefits__item text-body f-300"><strong>Kostenfreies Cyber-Security-Training</strong> für alle Ihre Mitarbeitenden mit Online-Training, Erklär-Videos und einem Wissenstest</li>
						<li class="double-benefits__item text-body f-300"><strong>Bereitstellung einer datenschutzkonformen Softwareplattform</strong> zur Prüfung von potenziell infizierten E-Mails und eines Werkzeugkastens für eine sichere Passwort-Programmierung</li>
					</ul>
					<!-- List End -->
				</div>
				<!-- Column End -->
				<div class="double-benefits__column">
					<ul class="double-benefits__list list-reset">
						<li class="double-benefits__item text-body f-300"><strong>Wiederherstellungskosten</strong> (inkl. Hardware-Ersatz), Betriebsunterbrechungs- und Ertragsausfallschäden</li>
						<li class="double-benefits__item text-body f-300"><strong>Mitversicherung von Drittschäden</strong>, z. B. Abwehr unberechtigter Schadenersatzansprüche</li>
						<li class="double-benefits__item text-body f-300"><strong>Übernahme von gesetzeskonformen Bußgeldern</strong> „Bring your own device“-Deckung z. B. berufliche Nutzung privater Smartphones</li>
						<li class="double-benefits__item text-body f-300"><strong>Betriebsunterbrechung</strong> zur Sicherung Ihres Umsatzes – Dies gilt auch bei technischen Störungen</li>
						<li class="double-benefits__item text-body f-300"><strong>Erweiterung der Betriebsunterbrechungs-Leistung</strong> um Mehrkosten</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
