<section class="offer-row__wrapper">
	<div class="offer-row">
		<div class="offer-row__header wrapper-full">
			<h4 class="offer-row__title text-header f-300 color-lightblue">Sie haben bereits ein “Produkt”?</h4>
		</div>
		<div class="offer-row__container">
			<div class="offer-row__segment wrapper-full">
				<div class="offer-row__flex d-flex">
					<div class="offer-row__image">
						<img src="https://via.placeholder.com/400x360" alt="" class="offer-row__img d-block">
					</div>
					<div class="offer-row__content d-flex">
						<div class="offer-row__text">
							<h5 class="offer-row__subtitle text-subheader f-600">Gerne überprüfen wir für Sie Ihren Versicherungsschutz, auch für „das Produkt“.</h5>
							<p class="offer-row__copy text-body f-300">Das „(Detail-)Produkt“ wird individuell auf Ihr Behandlungsspektrum abgestimmt. Hierbei wird auch das jeweilige Behandlungsrisiko entsprechend Ihrer Fachrichtung bewertet. Eine Onlineberechnung eines „(Detail-)Produkts“  kann aus diesem Grund zu völlig falschen Ergebnissen gelangen, welche im Schadenfall sehr teurer werden können, wenn Risiken nicht ausreichend versichert sind. Sie erhalten von uns einen hoch professionellen Vorschlag für Ihr „(Detail-)Produkt“.</p>
						</div>
						<div class="offer-row__cta d-flex">
							<a href="#" class="offer-row__btn u-btn text-benefits">Vergleichsangebot einholen</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
