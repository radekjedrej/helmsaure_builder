<section class="svg-cards__wrapper svg-cards--produkte">
    <div class="svg-cards wrapper-full">
		<h4 class="svg-cards__main__title text-header f-300">Das könnte Sie auch interessieren</h4>
		<div class="svg-cards__container wrapper-small">
			<div class="svg-cards__row d-flex d-flex-wrap">

				<!-- Column Start -->
				<div class="svg-cards__column">
					<a href="#" class="svg-cards__segment d-block">
						<div class="svg-cards__top d-flex">
							<div class="svg-cards__svg">
								<svg width="67" height="72" viewBox="0 0 67 72" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33.4161 71.5352C33.2846 71.5352 33.2846 71.5352 33.1531 71.5352C20.0032 66.0122 10.0094 53.7829 4.48642 36.1621C0.40997 22.8807 0.146973 11.0459 0.146973 10.9144C0.146973 10.6514 0.278471 10.3884 0.672967 10.2569L33.2846 0C33.4161 0 33.5476 0 33.6791 0L66.2907 10.2569C66.5537 10.3884 66.8167 10.6514 66.8167 10.9144C66.8167 11.0459 66.5537 22.8807 62.4772 36.1621C56.9543 53.7829 46.9604 66.0122 33.6791 71.5352C33.6791 71.5352 33.5476 71.5352 33.4161 71.5352ZM1.59346 11.4404C1.72495 16.4373 4.35492 57.8593 33.4161 70.2202C62.6087 57.8593 65.2387 16.4373 65.3702 11.4404L33.4161 1.44648L1.59346 11.4404Z" fill="#fff"/><path d="M33.4158 63.6453C33.2843 63.6453 33.2843 63.6453 33.1528 63.6453C8.43108 53.257 7.64209 16.9634 7.64209 16.5689C7.64209 16.3059 7.77359 16.0429 8.16808 15.9114L33.4158 8.02148C33.5473 8.02148 33.6788 8.02148 33.8103 8.02148L59.058 15.9114C59.321 16.0429 59.584 16.3059 59.584 16.5689C59.584 16.9634 58.795 53.257 33.9418 63.5138C33.6788 63.5138 33.5473 63.6453 33.4158 63.6453ZM8.95707 17.0949C9.08857 21.4343 11.324 52.731 33.4158 62.1988C55.639 52.731 57.743 21.4343 58.006 17.0949L33.4158 9.33647L8.95707 17.0949Z" fill="#fff"/><path d="M31.049 49.049C29.997 49.049 29.0765 48.6545 28.2875 47.8655L18.5566 38.0032L23.9481 32.6117L30.523 39.3181L45.6453 19.0674L51.8258 23.6698L34.205 47.471C33.5475 48.3915 32.4955 48.9175 31.4435 49.049C31.312 49.049 31.1805 49.049 31.049 49.049ZM20.5291 38.0032L29.3395 46.9451C29.8655 47.471 30.523 47.734 31.312 47.734C31.9695 47.734 32.627 47.3395 33.153 46.6821L50.1163 23.9328L46.1713 21.0399L30.786 41.4221L24.0796 34.5842L20.5291 38.0032Z" fill="#fff"/></svg>
							</div>
							<div class="svg-cards__bg"></div>
                		</div>
						<div class="svg-cards__oblique d-flex">
							<div class="svg-cards__box">
								<div class="svg-cards__content">
									<h5 class="svg-cards__title text-header-three f-300">Rechtsschutzversicherung</h5>
									<p class="svg-cards__copy text-body-sm-2 f-600">Beste Absicherung für Rechts-streite. Damit Sie schnell eine Lösung finden können.</p>
								</div>
							</div>
							<div class="svg-cards__triangle">
								<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
							</div>
						</div>
					</a>
					<ul class="svg-cards__list d-block list-reset">
						<li class="svg-cards__item text-body-sm f-300">Erstattung von Gerichtskosten, Anwaltshonoraren und Vorschüssen</li>
						<li class="svg-cards__item text-body-sm f-300">Beratung durch unabhängige Anwälte</li>
						<li class="svg-cards__item text-body-sm f-300">Mitversicherung von Mitarbeitenden</li>
					</ul>
				</div>
				<!-- Column End -->
				
				<div class="svg-cards__column">
					<a href="#" class="svg-cards__segment d-block">
						<div class="svg-cards__top d-flex">
							<div class="svg-cards__svg">
								<svg width="67" height="72" viewBox="0 0 67 72" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33.4161 71.5352C33.2846 71.5352 33.2846 71.5352 33.1531 71.5352C20.0032 66.0122 10.0094 53.7829 4.48642 36.1621C0.40997 22.8807 0.146973 11.0459 0.146973 10.9144C0.146973 10.6514 0.278471 10.3884 0.672967 10.2569L33.2846 0C33.4161 0 33.5476 0 33.6791 0L66.2907 10.2569C66.5537 10.3884 66.8167 10.6514 66.8167 10.9144C66.8167 11.0459 66.5537 22.8807 62.4772 36.1621C56.9543 53.7829 46.9604 66.0122 33.6791 71.5352C33.6791 71.5352 33.5476 71.5352 33.4161 71.5352ZM1.59346 11.4404C1.72495 16.4373 4.35492 57.8593 33.4161 70.2202C62.6087 57.8593 65.2387 16.4373 65.3702 11.4404L33.4161 1.44648L1.59346 11.4404Z" fill="#fff"/><path d="M33.4158 63.6453C33.2843 63.6453 33.2843 63.6453 33.1528 63.6453C8.43108 53.257 7.64209 16.9634 7.64209 16.5689C7.64209 16.3059 7.77359 16.0429 8.16808 15.9114L33.4158 8.02148C33.5473 8.02148 33.6788 8.02148 33.8103 8.02148L59.058 15.9114C59.321 16.0429 59.584 16.3059 59.584 16.5689C59.584 16.9634 58.795 53.257 33.9418 63.5138C33.6788 63.5138 33.5473 63.6453 33.4158 63.6453ZM8.95707 17.0949C9.08857 21.4343 11.324 52.731 33.4158 62.1988C55.639 52.731 57.743 21.4343 58.006 17.0949L33.4158 9.33647L8.95707 17.0949Z" fill="#fff"/><path d="M31.049 49.049C29.997 49.049 29.0765 48.6545 28.2875 47.8655L18.5566 38.0032L23.9481 32.6117L30.523 39.3181L45.6453 19.0674L51.8258 23.6698L34.205 47.471C33.5475 48.3915 32.4955 48.9175 31.4435 49.049C31.312 49.049 31.1805 49.049 31.049 49.049ZM20.5291 38.0032L29.3395 46.9451C29.8655 47.471 30.523 47.734 31.312 47.734C31.9695 47.734 32.627 47.3395 33.153 46.6821L50.1163 23.9328L46.1713 21.0399L30.786 41.4221L24.0796 34.5842L20.5291 38.0032Z" fill="#fff"/></svg>
							</div>
							<div class="svg-cards__bg"></div>
                		</div> 
						<div class="svg-cards__oblique d-flex">
							<div class="svg-cards__box">
								<div class="svg-cards__content">
									<h5 class="svg-cards__title text-header-three f-300">Gebäudeversicherung</h5>
									<p class="svg-cards__copy text-body-sm-2 f-600">Damit Ihre gewerbliche Immobilie nicht unvorhersehbaren Risiken zum Opfer fällt.</p>
								</div>
							</div>
							<div class="svg-cards__triangle">
								<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
							</div>
						</div>
					</a>
					<ul class="svg-cards__list d-block list-reset">
						<li class="svg-cards__item text-body-sm f-300">Sicherheit bei Feuer, Wasser oder Unwetter</li>
						<li class="svg-cards__item text-body-sm f-300">Schutz für das gesamte Gebäude inklusive seiner Bestandteile</li>
						<li class="svg-cards__item text-body-sm f-300">Kostenübernahme von Reparatur- und Wiederherstellungskosten</li>
					</ul>
				</div>

				<div class="svg-cards__column">
					<a href="#" class="svg-cards__segment d-block">
						<div class="svg-cards__top d-flex">
							<div class="svg-cards__svg">
								<svg width="67" height="72" viewBox="0 0 67 72" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M33.4161 71.5352C33.2846 71.5352 33.2846 71.5352 33.1531 71.5352C20.0032 66.0122 10.0094 53.7829 4.48642 36.1621C0.40997 22.8807 0.146973 11.0459 0.146973 10.9144C0.146973 10.6514 0.278471 10.3884 0.672967 10.2569L33.2846 0C33.4161 0 33.5476 0 33.6791 0L66.2907 10.2569C66.5537 10.3884 66.8167 10.6514 66.8167 10.9144C66.8167 11.0459 66.5537 22.8807 62.4772 36.1621C56.9543 53.7829 46.9604 66.0122 33.6791 71.5352C33.6791 71.5352 33.5476 71.5352 33.4161 71.5352ZM1.59346 11.4404C1.72495 16.4373 4.35492 57.8593 33.4161 70.2202C62.6087 57.8593 65.2387 16.4373 65.3702 11.4404L33.4161 1.44648L1.59346 11.4404Z" fill="#fff"/><path d="M33.4158 63.6453C33.2843 63.6453 33.2843 63.6453 33.1528 63.6453C8.43108 53.257 7.64209 16.9634 7.64209 16.5689C7.64209 16.3059 7.77359 16.0429 8.16808 15.9114L33.4158 8.02148C33.5473 8.02148 33.6788 8.02148 33.8103 8.02148L59.058 15.9114C59.321 16.0429 59.584 16.3059 59.584 16.5689C59.584 16.9634 58.795 53.257 33.9418 63.5138C33.6788 63.5138 33.5473 63.6453 33.4158 63.6453ZM8.95707 17.0949C9.08857 21.4343 11.324 52.731 33.4158 62.1988C55.639 52.731 57.743 21.4343 58.006 17.0949L33.4158 9.33647L8.95707 17.0949Z" fill="#fff"/><path d="M31.049 49.049C29.997 49.049 29.0765 48.6545 28.2875 47.8655L18.5566 38.0032L23.9481 32.6117L30.523 39.3181L45.6453 19.0674L51.8258 23.6698L34.205 47.471C33.5475 48.3915 32.4955 48.9175 31.4435 49.049C31.312 49.049 31.1805 49.049 31.049 49.049ZM20.5291 38.0032L29.3395 46.9451C29.8655 47.471 30.523 47.734 31.312 47.734C31.9695 47.734 32.627 47.3395 33.153 46.6821L50.1163 23.9328L46.1713 21.0399L30.786 41.4221L24.0796 34.5842L20.5291 38.0032Z" fill="#fff"/></svg>
							</div>
							<div class="svg-cards__bg"></div>
                		</div>
						<div class="svg-cards__oblique d-flex">
							<div class="svg-cards__box">
								<div class="svg-cards__content">
									<h5 class="svg-cards__title text-header-three f-300">Kraftfahrzeugversicherung</h5>
									<p class="svg-cards__copy text-body-sm-2 f-600">Gesetzlich vorgeschrieben aber durch Zusatzleistungen individuell kompossibel.</p>
								</div>
							</div>
							<div class="svg-cards__triangle">
									<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
							</div>
						</div> 
					</a>
					<ul class="svg-cards__list d-block list-reset">
						<li class="svg-cards__item text-body-sm f-300">Ihr Firmenwagen in besten Händen, ob  Haftpflicht-, Teilkasko- oder Kaskoversicherung</li>
						<li class="svg-cards__item text-body-sm f-300">Schnelle Schadenregulierung, auch im Ausland</li>
						<li class="svg-cards__item text-body-sm f-300">Deckt ebenfalls Wildtierschäden ab</li>
					</ul>
				</div>

			</div>
		</div>
    </div>
</section>
