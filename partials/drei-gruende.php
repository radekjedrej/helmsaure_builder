<section class="three-steps__wrapper">
	<div class="three-steps">
		<div class="three-steps__container wrapper-small">
			<div class="three-steps__oblique d-flex">
				<div class="three-steps__box">
					<div class="three-steps__content">
						<div class="three-steps__line text-header f-600">Genau richtig:</div>
						<h4 class="three-steps__title text-header f-300">Drei Gründe für unsere Cyberversicherung</h4>
					</div>
				</div>
				<div class="three-steps__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
				</div>
			</div>
			<div class="three-steps__row d-flex">

				<!-- Column Start -->
				<div class="three-steps__column">
					<div class="three-steps__headline d-flex">
						<div class="three-steps__svg">
							<svg viewBox="0 0 17 49" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.193 0.0722656H13.3805L0.327637 9.80784L2.20264 12.1155C8.62091 7.06746 12.0103 4.4713 12.3709 4.11073C12.7315 3.82227 13.0921 3.46169 13.3805 3.17323C13.0921 6.56265 13.0199 9.87996 13.0199 13.053V48.029H16.193V0.0722656Z" fill="#00ADF0"/></svg>
						</div>
						<div class="three-steps__heading">
							<h5 class="three-steps__title text-benefits f-600">Unverzügliche Hilfestellung rund um die Uhr</h5>
						</div>
					</div>
					<div class="three-steps__copy text-body f-300">
						<p>Im Schadenfall garantieren wir Ihnen eine unverzügliche Hilfestellung durch unsere Fachabteilungen. Wir lassen Sie nicht im Regen stehen!</p>
					</div>
				</div>
				<!-- Column End -->

				<div class="three-steps__column">
					<div class="three-steps__headline d-flex">
						<div class="three-steps__svg">
							<svg viewBox="0 0 30 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.7117 44.9279H4.39919V44.7837L16.5146 32.3077C20.6973 28.0529 23.5819 24.4471 25.1684 21.4904C26.755 18.6058 27.5482 15.5769 27.5482 12.4038C27.5482 8.50962 26.3944 5.48077 24.0146 3.31731C21.6348 1.15385 18.3896 0 14.279 0C9.51938 0.0721154 5.12034 1.73077 1.22611 5.04808L2.95688 7.28365C6.56265 4.39904 10.3126 2.95673 14.279 2.95673C17.2357 2.95673 19.6155 3.82212 21.4905 5.625C23.3655 7.42788 24.303 9.66346 24.303 12.5481C24.303 14.4952 23.9425 16.3702 23.2934 18.101C22.6444 19.9038 21.5627 21.7067 20.1925 23.6538C18.8223 25.601 16.2982 28.4135 12.7646 32.0913L0.0722656 45.1442V47.9567H29.6396V44.9279H29.7117Z" fill="#00ADF0"/></svg>
						</div>
						<div class="three-steps__heading">
							<h5 class="three-steps__title text-benefits f-600">Mitversicherung von Eigenschäden, Vermögens-schäden, Wiederherstel-lungskosten, Drittschäden, Betriebsunterbrechung etc.</h5>
						</div>
					</div>
					<div class="three-steps__copy text-body f-300">
						<p>Inkl Hardware-Ersatz und Ertragsausfall-schäden. Außerdem übernehmen wir gesetzteskonforme Bußgelder.</p>
					</div>
				</div>

				<div class="three-steps__column">
					<div class="three-steps__headline d-flex">
						<div class="three-steps__svg">
							<svg viewBox="0 0 30 49" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M24.88 3.17313C22.5723 1.1539 19.255 0.144287 14.928 0.144287C12.3319 0.144287 9.73573 0.576979 7.21169 1.44236C4.68765 2.30775 2.45207 3.53371 0.649189 5.04813L2.23573 7.2116C4.61554 5.62506 6.779 4.47121 8.654 3.82217C10.6011 3.17313 12.6925 2.88467 15.0002 2.88467C17.9569 2.88467 20.4088 3.67794 22.2117 5.19236C24.0146 6.7789 24.9521 8.79813 24.9521 11.3943C24.9521 14.5674 23.7982 17.0193 21.5627 18.8943C19.3271 20.6972 16.2261 21.6347 12.3319 21.6347H7.21169V24.6635H12.2598C16.9473 24.6635 20.4809 25.4568 22.8607 27.1154C25.2405 28.7741 26.4665 31.1539 26.4665 34.2549C26.4665 37.7164 25.3127 40.3847 22.9328 42.3318C20.553 44.2068 17.0194 45.2164 12.3319 45.2164C10.3848 45.2164 8.29342 44.9279 6.05784 44.4231C3.82227 43.9183 1.80304 43.1251 0.0722656 42.2597V45.4327C3.60592 47.1635 7.7165 48.101 12.4761 48.101C18.029 48.101 22.2838 46.8751 25.2405 44.4952C28.1973 42.1154 29.7117 38.726 29.7117 34.3991C29.7117 31.1539 28.7021 28.6299 26.755 26.6827C24.8078 24.7356 21.9232 23.5818 18.2453 23.1491V22.9327C21.2742 22.3558 23.7261 21.1299 25.529 19.1106C27.3319 17.1635 28.2694 14.7116 28.2694 11.827C28.4136 8.07698 27.2598 5.19236 24.88 3.17313Z" fill="#00ADF0"/></svg>
						</div>
						<div class="three-steps__heading">
							<h5 class="three-steps__title text-benefits f-600">Kostenfreies Cyber-Security-Training für alle Mitarbei-tenden</h5>
						</div>
					</div>
					<div class="three-steps__copy text-body f-300">
						<p>Mit Online-Training und Erklär-Videos und einem Wissenstest sowie Bereitstellung einer datenschutz-konformen Softwareplattform zur Prüfung von potenziell infizierten E-Mails und eines Instruments für eine sichere Passwort-Programmierung.</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
