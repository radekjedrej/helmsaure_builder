<section class="announcement-list__wrapper wrapper-small">
    <div class="announcement-list">
        <div class="announcement-list__top d-flex">
            <div class="announcement-list__svg">
				<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.3 49.3"><rect width="50.3" height="49.3" style="fill:none"/><path d="M36.8,0c-1.7,0-3.5,1.4-5.5,4.3-3.7,5.4-14.1,6-18.4,6-1.9,0-3.2-.1-3.2-.1H9.6C4.3,10.2,0,15.3,0,21.7S3.6,32.4,8.9,32.4c4.6.1,18.1,2.4,22.3,7.5,1.9,2.3,3.9,3.6,5.8,3.6a3.21,3.21,0,0,0,2.7-1.3,5.16,5.16,0,0,0,1.1-3.4V5.3h0a8.56,8.56,0,0,0-.7-2.9A3.94,3.94,0,0,0,36.8,0Zm0,1.9c1.9,0,2.4,3.4,2.4,3.4h0V38.8c0,1.8-.9,2.8-2.2,2.8s-3-.9-4.7-3c-5.6-6.8-23.4-8.1-23.4-8.1-4.4,0-7.3-3.5-7.3-8.8s3.6-9.6,8-9.6c0,0,1.3.1,3.4.1,5.4,0,15.7-.8,19.7-6.7,1.8-2.7,3.1-3.6,4.1-3.6" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M18.5,33.6a.88.88,0,0,1-.8-.9v-19a.81.81,0,1,1,1.6-.25,1,1,0,0,1,0,.25V32.6c0,.6-.3,1-.8,1" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M34,42.1a.88.88,0,0,1-.8-.9v-19A.81.81,0,1,1,34.8,22a1,1,0,0,1,0,.25V41.1c0,.6-.3,1-.8,1" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M9.6,31a1.63,1.63,0,0,0-1.1.5A2.69,2.69,0,0,0,8,33c.4,6.2,1.8,11.7,4,15.5a1.56,1.56,0,0,0,1.3.8h5.2a1.56,1.56,0,0,0,1.3-.8,2.23,2.23,0,0,0,.2-1.8,41,41,0,0,1-2-12.9,1.79,1.79,0,0,0-1.4-1.9L9.8,31H9.6m0,1.9,6.8.9a41.4,41.4,0,0,0,2.2,13.6H13.4C11.3,44,9.9,38.8,9.6,32.9" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M49.5,23.6H43.8a.91.91,0,0,1,0-1.8h5.7a.88.88,0,0,1,.8.9.83.83,0,0,1-.74.9H49.5" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M43.8,13.2a.62.62,0,0,1-.6-.4,1,1,0,0,1,.2-1.3l4-3.6a.73.73,0,0,1,1,.1.86.86,0,0,1,.07.1,1,1,0,0,1-.2,1.3l-4,3.6a.54.54,0,0,1-.5.2" style="fill:#00adf0"/><rect width="50.3" height="49.3" style="fill:none"/><path d="M47.8,36.8c-.2,0-.3-.1-.5-.2l-4-3.6a1.23,1.23,0,0,1-.2-1.3.73.73,0,0,1,1-.27l.1.07,4,3.6a1.23,1.23,0,0,1,.2,1.3.81.81,0,0,1-.6.4" style="fill:#00adf0"/></svg>
            </div>
            <div class="announcement-list__header">
                <div class="announcement-list__title text-header f-300">Aktuelles!</div>
            </div>
        </div>
        <div class="announcement-list__bottom d-flex">
            <div class="announcement-list__content">
                <div class="announcement-list__content__title text-subheader f-600">Headline Aktuelles: Diese Neuigkeiten gibt es im Bereich Gesundheitswesen</div>
                <div class="announcement-list__copy text-body f-300">
                    <p>Qui aut ea cus qui acest ratem recus sam faccum lab inum estin conse vendundes ne quis erum qui officillabo. Venducitatem aut facepel eatquam secti amus por accuptate comnihi liquiam. Cae sequas secea coratec eptatibus, ut que volorit ibuscil etur? Ro quatur? Aquam, sum eos dolo-ribus nimo del intia denis voluptatque molestibus mil int qui occatur aut es estia niae res modi doluptatia quae. Nim eiciend ellaboriorum facepudis sam se illore perrovit expland.</p>
                </div>
                <div class="announcement-list__more color-lightblue text-body f-600">weiterlesen</div>
            </div>
            <div class="announcement-list__benefits">
                <ul class="announcement-list__elements list-reset">
					<li class="announcement-list__item text-body f-300">Maßgeschneiderte und leistungsstarke Konzepte</li>
					<li class="announcement-list__item text-body f-300">Flexibilität in der Bearbeitung</li>
					<li class="announcement-list__item text-body f-300">Optimierung der Prozesse speziell für Verbände</li>
					<li class="announcement-list__item text-body f-300">Eigene Zeichnungskapazitäten</li>
					<li class="announcement-list__item text-body f-300">Höherer Einfluss auf die Schadenregu-lierung</li>
				</ul>
            </div>
        </div>
    </div>
</section>
