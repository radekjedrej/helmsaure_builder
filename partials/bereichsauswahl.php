<section class="info-cards__wrapper wrapper-full">
    <div class="info-cards">
        <div class="info-cards__row d-flex d-flex-wrap">

			<!-- Column Start -->
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Ärzte</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Ärzte bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Ärzte bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Abrechnung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>
			<!-- Column End -->

            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">MVZ</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für MVZ bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für MVZ bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Abrechnung</a>
					</li>
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Navigator</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Krankenhäuser</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Krankenhäuser bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Krankenhäuser bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Abrechnung</a>
					</li>
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Navigator</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Praxiskliniken</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Praxiskliniken bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Praxiskliniken bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Abrechnung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

			<div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Pflegeheime</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Pflegeheime bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>	
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Pflegeheime bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

			<div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Physiotherapeuten</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Physiotherapeuten bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Physiotherapeuten bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

			<div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Medizinisches Personal</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für medizinische Fachangestellte bieten wir optimierte Konzepte und Leistungen.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für medizinische Fachangestellte bieten wir optimierte Konzepte und Leistungen.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

			<div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Kooperation mit Fachver-bänden</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Wir kooperieren mit zahlreichen Verbänden aus Ihrem Fachbereich.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Wir kooperieren mit zahlreichen Verbänden aus Ihrem Fachbereich.</h3>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                </ul>
            </div>

        </div>
    </div>
</section>
