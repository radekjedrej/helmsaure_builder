<section class="info-cards__wrapper wrapper-small info-cards--highlights">
    <div class="info-cards">
		<h4 class="info-cards__main__title text-header f-300">Highlights für unsere Kunden</h4>
        <div class="info-cards__row d-flex d-flex-wrap">

			<!-- Column Start -->
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h5 class="info-cards__title text-header-three f-300">Branchenberatung</h5>
								<p class="info-cards__copy text-body-sm-2 f-600">Ihr Wohl ist unser Ziel.</p>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">Persönlich und vor Ort durch ein erfahrenes Expertenteam</li>
                    <li class="info-cards__item text-body-sm f-300">Jahrzehntelange Erfahrung speziell in Ihrer Branche</li>
                    <li class="info-cards__item text-body-sm f-300">Individueller Service dank Maklerstatus</li>
                </ul>
            </div>
			<!-- Column End -->

            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h5 class="info-cards__title text-header-three f-300">Digitale Kundenakte</h5>
								<p class="info-cards__copy text-body-sm-2 f-600">Ihre IT-Lösung für mehr Überblick.</p>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">Zentrale Ablage Ihrer Dokumente</li>
                    <li class="info-cards__item text-body-sm f-300">Sichere und einfache Bedienung</li>
					<li class="info-cards__item text-body-sm f-300">Ihr direkter Draht zu uns</li>
                </ul>
            </div>

            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h5 class="info-cards__title text-header-three f-300">Helmsauer Assekuradeur</h5>
								<p class="info-cards__copy text-body-sm-2 f-600">Immer einen Schritt voraus.</p>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <ul class="info-cards__list d-block list-reset">
					<li class="info-cards__item text-body-sm f-300">Flexibilität in der Bearbeitung</li>
                    <li class="info-cards__item text-body-sm f-300">Optimierung der Prozesse speziell für Verbände</li>
					<li class="info-cards__item text-body-sm f-300">Eigene Zeichnungskapazitäten</li>
                </ul>
            </div>

        </div>
    </div>
</section>
