<section class="select-box__wrapper">
	<div class="select-box wrapper-full">
		<div class="select-box__container">
			<div class="select-box__box">
				<div class="select-box__desc">
					<p>Jetzt Tätigkeit wählen und spezialisierte Konzepte erhalten:</p>
					<select class="select-box__list">
						<option>Jungarzt</option>
						<option>Allgemeinmedizin</option>
						<option>Anästesiologie</option>
						<option>Arbeits- und Umweltmedizin</option>
						<option>Augenheilkunde</option>
						<option>Chirugie</option>
						<option>Frauenheilkunde und Geburtshilfe</option>
						<option>Hals-Nasen-Ohrenheilkunfe</option>
						<option>Haut- und Geschlechtskrankheiten</option>
						<option>Humangenetik</option>
						<option>Innere und Allgemeinmedizin (Hausarzt)</option>
						<option>Innere Medizin (fachärztlich)</option>
						<option>Kinder- und Jugendmedizin</option>
						<option>Kinder- und Jugendpsychiatrie und -psychotherapie</option>
					</select>
				</div>
			</div>
		</div>
	</div>
</section>
