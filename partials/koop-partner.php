<section class="resources-carousel__wrapper wrapper-full">
	<div class="resources-carousel">
		<h4 class="resources-carousel__title text-subheader f-600">Unsere Kooperationspartner in Ihrem Fachbereich</h4>
		<div class="resources-carousel__arrow resources-carousel__left">
			<svg viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M16.0756 1.88245L2.1543 15.9695L16.0756 30.0566" stroke="#00ADF0" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</div>
		<div class="resources-carousel__arrow resources-carousel__right">
			<svg viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M1.75879 30.0565L15.8459 15.9694L1.75879 2.04803" stroke="#00ADF0" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</div>
		<div class="resources-carousel__container wrapper-small">
			<!-- Carousel Start -->
			<div class="resources-carousel__carousel swiper">
				<div class="resources-carousel__row swiper-wrapper">

					<!-- Slide Start -->
					<a href="#" class="resources-carousel__slide swiper-slide d-block">
						<div class="resources-carousel__image">
							<img src="/assets/public/images/Verband_BHAEV.png" alt="" class="resources-carousel__img">
						</div>
						<div class="resources-carousel__text">
							<div class="resources-carousel__name text-body f-300">Bayerischer Hausärzteverband e.V.</div>
							<div class="resources-carousel__page text-body f-300 color-lightblue">www.hausaerzte-bayern.de</div>
						</div>
					</a>
					<!-- Slide End -->

					<a href="#" class="resources-carousel__slide swiper-slide d-block">
						<div class="resources-carousel__image">
							<img src="/assets/public/images/Verband_AFB.png" alt="" class="resources-carousel__img">
						</div>
						<div class="resources-carousel__text">
							<div class="resources-carousel__name text-body f-300">AFB e.V. - Allianz fachärztlicher Berufsverbände</div>
							<div class="resources-carousel__page text-body f-300 color-lightblue">www.afb-fachaerzte-bayern.de/</div>
						</div>
					</a>

					<a href="#" class="resources-carousel__slide swiper-slide d-block">
						<div class="resources-carousel__image">
							<img src="/assets/public/images/Verband_BFAV.png" alt="" class="resources-carousel__img">
						</div>
						<div class="resources-carousel__text">
							<div class="resources-carousel__name text-body f-300">Bayerischer Facharztverband e.V.</div>
							<div class="resources-carousel__page text-body f-300 color-lightblue">www.bayerischerfacharztverbund.de/</div>
						</div>
					</a>

					<a href="#" class="resources-carousel__slide swiper-slide d-block">
						<div class="resources-carousel__image">
							<img src="/assets/public/images/Verband_BHAEV.png" alt="" class="resources-carousel__img">
						</div>
						<div class="resources-carousel__text">
							<div class="resources-carousel__name text-body f-300">Bayerischer Hausärzteverband e.V.</div>
							<div class="resources-carousel__page text-body f-300 color-lightblue">www.hausaerzte-bayern.de</div>
						</div>
					</a>

					<a href="#" class="resources-carousel__slide swiper-slide d-block">
						<div class="resources-carousel__image">
							<img src="/assets/public/images/Verband_BHAEV.png" alt="" class="resources-carousel__img">
						</div>
						<div class="resources-carousel__text">
							<div class="resources-carousel__name text-body f-300">Bayerischer Hausärzteverband e.V.</div>
							<div class="resources-carousel__page text-body f-300 color-lightblue">www.hausaerzte-bayern.de</div>
						</div>
					</a>

				</div>
			</div>
			<!-- Carousel End -->
		</div>
	</div>
</section>
