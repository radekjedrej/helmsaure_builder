<section class="video-row__wrapper">
	<div class="video-row wrapper-small">
		<div class="video-row__container d-flex">
			<div class="video-row__responsive">
				<iframe class="video-row__iframe" src="https://player.vimeo.com/video/253989945?h=c6db007fe5" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section>
