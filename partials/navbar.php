<header class="l-navbar">
    <div class="l-navbar__bar">
        <a href="#" class="l-navbar__myhelmsauer d-flex">
            <div class="l-navbar__padlock">
				<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 354 495.53"><rect y="0.03" width="353.9" height="495.5" style="fill:none"/><path d="M251.3,194.63H102.5v-80a74.43,74.43,0,0,1,148.8,0v80Zm-42.4,167.2V447H145v-85.2a65.55,65.55,0,0,1-20.5-20.4,61.41,61.41,0,1,1,84.42,20.39h0M303,194.63v-70.7C301.7,54.13,244.2-1.37,174.6,0,106.9,1.33,52.3,56,51,123.93v70.7H0v300.8H353.9V194.63Z" transform="translate(0.1 0)" style="fill:#fff"/></svg>
            </div>
            <div class="l-navbar__login color-white f-400 text-login">myHELMSAUER</div>
        </a>
        <div class="wrapper-full">
            <div class="l-navbar__container d-flex">
                <div class="l-navbar__logo d-flex">
                    <a class="l-navbar__image d-block" href="index.html" target="">
                        <img class="l-navbar__img d-block" src="/assets/public/images/logo.png">
                    </a>
                </div>
        
                <!-- Desktop Menu -->
                <div class="l-navbar__menu">
                    <div class="l-navbar__navigation">
                        <ul class="l-navbar__links d-flex list-reset">
                            <li class="l-navbar__item">
                                <div class="l-navbar__content">
                                    <a href="#" class="l-navbar__link text-links f-400">Gesundheitswesen</a>
                                    <div class="l-navbar__dropdown">
                                        <ul class="l-navbar__dropdown__container list-reset">
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Ärzte</a>
                                                <div class="l-navbar__submenu">
                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Versicherung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Arzthaftpflichtversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Cyberversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisversicherungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Rechtschutzersicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Altersvorsorge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Krankenversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Berufsunfähigkeitsversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Existenzschutz</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Kraftfahrzeugversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Private Risiken</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Abrechnung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Privatpatienten</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivverträge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivvertragsangebote</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Online Portale</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Beratung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxis-/Stellenbörse</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisübernahme</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisbewertung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebswirtschaftliche Beratung</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Bildungsangebot</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">E-Learnings</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Lehr- und Studiengänge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Seminare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Tagungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Webinare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Workshops</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">MVZ</a>
                                                <div class="l-navbar__submenu">
                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Test</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Arzthaftpflichtversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Cyberversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisversicherungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Rechtschutzersicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Altersvorsorge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Krankenversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Berufsunfähigkeitsversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Existenzschutz</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Kraftfahrzeugversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Private Risiken</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Abrechnung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Privatpatienten</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivverträge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivvertragsangebote</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Online Portale</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Beratung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxis-/Stellenbörse</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisübernahme</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisbewertung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebswirtschaftliche Beratung</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Bildungsangebot</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">E-Learnings</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Lehr- und Studiengänge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Seminare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Tagungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Webinare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Workshops</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Krankenhäuser</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Praxiskliniken</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Pflegeheime</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Physiotherapeuten</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Medizinisches Personal</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Kooperation mit Fachverbänden</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="l-navbar__item">
                                <div class="l-navbar__content">
                                    <a class="l-navbar__link text-links f-400" href="#" target="">Bildungsangebote</a>
									<div class="l-navbar__dropdown">
                                        <ul class="l-navbar__dropdown__container list-reset">
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Test</a>
                                                <div class="l-navbar__submenu">
                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Versicherung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Arzthaftpflichtversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Cyberversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisversicherungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Rechtschutzersicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Altersvorsorge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Krankenversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Berufsunfähigkeitsversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Existenzschutz</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Kraftfahrzeugversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Private Risiken</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Abrechnung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Privatpatienten</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivverträge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivvertragsangebote</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Online Portale</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Beratung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxis-/Stellenbörse</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisübernahme</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisbewertung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebswirtschaftliche Beratung</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Bildungsangebot</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">E-Learnings</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Lehr- und Studiengänge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Seminare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Tagungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Webinare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Workshops</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">MVZ</a>
                                                <div class="l-navbar__submenu">
                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Test</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Arzthaftpflichtversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Cyberversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisversicherungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Rechtschutzersicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Altersvorsorge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebliche Krankenversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Berufsunfähigkeitsversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Existenzschutz</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Kraftfahrzeugversicherung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Private Risiken</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Abrechnung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Privatpatienten</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivverträge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Selektivvertragsangebote</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Online Portale</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Beratung</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxis-/Stellenbörse</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisübernahme</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Praxisbewertung</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Betriebswirtschaftliche Beratung</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>

                                                    <div class="l-navbar__column d-flex">
                                                        <h2 class="l-navbar__column__title color-white text-header-four">Bildungsangebot</h2>
                                                        <ul class="l-navbar__column__list list-reset">
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">E-Learnings</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Lehr- und Studiengänge</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Seminare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Tagungen</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Webinare</a>
                                                            </li>
                                                            <li class="l-navbar__column__item">
                                                                <a href="#" class="l-navbar__column__link text-links f-300 line-big">Workshops</a>
                                                            </li>
                                                        </ul>
                                                        <div class="l-navbar__column__show-all color-white text-links f=600">Alle anzeigen</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Krankenhäuser</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Praxiskliniken</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Pflegeheime</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Physiotherapeuten</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Medizinisches Personal</a>
                                            </li>
                                            <li class="l-navbar__dropdown__item text-links f-400">
                                                <a href="#" class="l-navbar__dropdown__link">Kooperation mit Fachverbänden</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="l-navbar__item">
                                <div class="l-navbar__content">
                                    <a class="l-navbar__link text-links f-400" href="#" target="">Über Helmsauer</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
				<div class="l-navbar__segment">
					<div class="l-navbar__burger">
						<div class="l-navbar__line"></div>
					</div>
				</div>
            </div>
        </div>
    </div>

    <!-- Mobile Menu -->
    <div class="m-menu wrapper-full">
        <div class="m-menu__container">
            <ul class="m-menu__list list-reset">
                <li class="m-menu__item">
                    <a href="#" class="m-menu__element m-menu__link d-block text-links color-white u-text-center f-300">Gesundheitswesen</a>
                    <ul class="m-menu__drop m-menu__dropdown list-reset">
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Ärzte</a>
                            <ul class="m-menu__drop m-menu__submenu list-reset">
                                <li class="m-menu__submenu__item">
                                    <a href="" class="m-menu__element m-menu__submenu__link d-block text-links color-white u-text-center f-300">Versicherung</a>
                                    <ul class="m-menu__drop m-menu__pulldown list-reset">
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Artzthaftpflichtversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Cyberversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Praxisversicherungen</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Rechtschutzversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Betriebliche Altersvorsorge</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Betriebliche Krankenversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Berufsunfähigkeitsversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Existenzversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Kraftfahrzeugversicherung</a>
                                        </li>
                                        <li class="m-menu__pulldown__item">
                                            <a href="#" class="m-menu__element m-menu__pulldown__link d-block text-links color-white u-text-center f-300">Private Risiken</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="m-menu__submenu__item">
                                    <a href="#" class="m-menu__element m-menu__submenu__link d-block text-links color-white u-text-center f-300">Abrechnung</a>
                                </li>
                                <li class="m-menu__submenu__item">
                                    <a href="#" class="m-menu__element m-menu__submenu__link d-block text-links color-white u-text-center f-300">Beratung</a>
                                </li>
                                <li class="m-menu__submenu__item">
                                    <a href="#" class="m-menu__element m-menu__submenu__link d-block text-links color-white u-text-center f-300">Bildungsangebot</a>
                                </li>
                            </ul>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">MVZ</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Krankenhäuser</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Praxiskliniken</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Pflegeheime</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Physiotherapeuten</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Medizinisches Personal</a>
                        </li>
                        <li class="m-menu__dropdown__item">
                            <a href="#" class="m-menu__element m-menu__dropdown__link d-block text-links color-white u-text-center f-300">Kooperation mit Fachverbänden</a>
                        </li>
                    </ul>
                </li>
                <li class="m-menu__item">
                    <a href="#" class="m-menu__element m-menu__link d-block text-links color-white u-text-center f-300">Bildungsangebote</a>
                </li>
                <li class="m-menu__item">
                    <a href="#" class="m-menu__element m-menu__link d-block text-links color-white u-text-center f-300">Über Helmsauer</a>
                </li>
            </ul>
        </div>
    </div>
</header>
