<section class="contact-info__wrapper wrapper-full">
    <div class="contact-info">
		<div class="contact-info__row d-flex">
			<div class="contact-info__image">
				<img class="contact-info__img d-block" src="https://via.placeholder.com/1400x1000" alt="">
			</div>
			<div class="contact-info__bar d-flex">
				<div class="contact-info__content d-flex">
					<div class="contact-info__headline">
						<div class="contact-info__title text-header f-300 color-white">Sie wünschen mehr Informationen oder eine persönliche Beratung?</div>
						<div class="contact-info__subtitle text-subheader f-600 color-white">Nehmen Sie gerne Kontakt zu uns auf:</div>
					</div>
					<div class="contact-info__contact d-flex">
						<div class="contact-info__details d-flex">
							<a class="contact-info__item d-flex" href="mailto:service@helmsauer-gruppe.de">
								<div class="contact-info__svg">
									<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.2 24.1"><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M2.8,1A1.79,1.79,0,0,0,1,2.78V21.3a1.79,1.79,0,0,0,1.78,1.8H21.3a1.79,1.79,0,0,0,1.8-1.78V2.8A1.79,1.79,0,0,0,21.32,1H2.8ZM21.4,24.1H2.8A2.8,2.8,0,0,1,0,21.3V2.8A2.8,2.8,0,0,1,2.8,0H21.3a2.8,2.8,0,0,1,2.8,2.8V21.3a2.66,2.66,0,0,1-2.5,2.8h-.2" transform="translate(0 0)" style="fill:#00adf0"/><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M12.1,20.1a8.15,8.15,0,0,1-5.8-2.5,7.47,7.47,0,0,1-2.2-5.8,8.12,8.12,0,0,1,7.7-7.7A8,8,0,0,1,18,6.7a7.78,7.78,0,0,1,2.1,5.4,6.76,6.76,0,0,1-.4,2.4,2.31,2.31,0,0,1-4.5-.7V12.1h1v1.7a1.38,1.38,0,0,0,2.7.4,7.46,7.46,0,0,0,.3-2.1,7.26,7.26,0,0,0-1.8-4.7,7.13,7.13,0,0,0-5.6-2.3,7,7,0,0,0-6.7,6.7,6.86,6.86,0,0,0,2,5.2,7,7,0,0,0,5.1,2.2v.9Z" transform="translate(0 0)" style="fill:#00adf0"/><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M11.9,17.1c-2.4,0-4.3-2.2-4.3-5s1.9-5,4.3-5,4.3,2.2,4.3,5h-1c0-2.2-1.5-4-3.3-4s-3.3,1.8-3.3,4,1.5,4,3.3,4a2.89,2.89,0,0,0,2.1-.9l.7.7a4,4,0,0,1-2.8,1.2" transform="translate(0 0)" style="fill:#00adf0"/></svg>
								</div>
								<div class="contact-info__text color-white text-contact f-600">service@helmsauer-gruppe.de</div>
							</a>
							<a class="contact-info__item d-flex" href="tel:09119292185">
								<div class="contact-info__svg">
									<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.61 24.6"><rect width="24.6" height="24.6" style="fill:none"/><path d="M5,1.8H5m0,0H5m0,0H5m.2,0Zm-.1,0a4,4,0,0,0-4,4h0v.1h0A18,18,0,0,0,18.9,23.6a4,4,0,0,0,4-4,2.74,2.74,0,0,0-.1-.9c0-.1-.1-.2-.1-.3l-6.9-1.5a3.1,3.1,0,0,0-.9,1.8.32.32,0,0,1-.3.3.3.3,0,0,1-.4,0,14.22,14.22,0,0,1-8.6-8.5v-.4c.2-.2.3-.3.4-.3a3.84,3.84,0,0,0,1.8-.9l-1.5-7H6a2.74,2.74,0,0,0-.9-.1h0M18.9,24.6A18.91,18.91,0,0,1,0,5.9V5.8H0A5.12,5.12,0,0,1,5.1.7h0A4.07,4.07,0,0,1,6.2.8a1.94,1.94,0,0,1,.7.2c.2.1.3.2.3.4L8.9,8.9a.74.74,0,0,1-.1.5,3.83,3.83,0,0,1-1.9,1.2,13.37,13.37,0,0,0,7.2,7.2,6.18,6.18,0,0,1,1.2-1.9.43.43,0,0,1,.5-.1l7.4,1.6c.2,0,.3.2.4.3a1.94,1.94,0,0,1,.2.7,4.83,4.83,0,0,1,.1,1.2,5,5,0,0,1-5,5" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M18.3,11.7h-1A4.12,4.12,0,0,0,15.1,8l.5-.9a5.19,5.19,0,0,1,2.7,4.6" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M14.1,7.6a4.86,4.86,0,0,0-1.2-.2v-1a7.52,7.52,0,0,1,1.5.2Z" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M24.6,10.6h-1a9.2,9.2,0,0,0-.5-2.9l1-.3a8.52,8.52,0,0,1,.5,3.2" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M22.7,6.6A9.45,9.45,0,0,0,14,1V0a10.54,10.54,0,0,1,9.6,6.2Z" transform="translate(0 0)" style="fill:#00adf0"/></svg>
								</div>
								<div class="contact-info__text color-white text-number f-600">0911-9292-185</div>
							</a>
						</div>
						<div class="contact-info__cta">
							<a class="contact-info__btn u-btn text-download" href="#">Kontaktanfrage</a>
						</div>
					</div>
				</div>
			</div>
			<div class="contact-info__triangle">
				<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
			</div>
		</div>
    </div>
</section>
