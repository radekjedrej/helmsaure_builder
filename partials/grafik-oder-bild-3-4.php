<section class="image-medium__wrapper">
	<div class="image-medium wrapper-small">
		<div class="image-medium__container d-flex">
			<div class="image-medium__image">
				<img src="assets/public/images/big-image.jpg" alt="" class="image-medium__img">
			</div>
		</div>
	</div>
</section>
