<section class="certificates-row__wrapper">
	<div class="certificates-row wrapper-full">
		<div class="certificates-row__title text-header f-300">Dienstleistung aus Leidenschaft</div>
		<div class="certificates-row__container d-flex">
			<div class="certificates-row__content">
				<div class="certificates-row__copy text-body-sm f-300">
					<p>Die inhabergeführte HELMSAUER Gruppe ist bereits seit 1963 erfolgreich tätig. Unsere hohe Spezialisierung in verschiedenen Branchen ermöglicht zielgerichtete und passgenaue Lösungen in den Bereichen Versicherung, Finanzdienstleistung, Beratung, Weiterbildung und IT-Service.</p>
					
					<p>Wir bieten ein breites und individuelles Portfolio, das aus den Erkenntnissen unserer langjährigen Erfahrung in Verbindung mit den Anforderungen unserer Kunden entsteht.</p>
					
					<p>Dabei nehmen wir Ihre Bedürfnisse persönlich – Ihnen stehen 400 qualifizierte und service-orientierte Mitarbeiter an 38 Standorten in Deutschland, Österreich und der Schweiz für Ihre Anliegen gerne, auch bei Ihnen vor Ort, zur Verfügung.</p>
				</div>
			</div>
			<div class="certificates-row__items d-flex">
				<div class="certificates-row__image">
					<img src="/assets/public/images/certificate2.png" alt="" class="certificates-row__img d-block">
				</div>
				<div class="certificates-row__image">
					<img src="/assets/public/images/certificate1.png" alt="" class="certificates-row__img d-block">
				</div>
				<div class="certificates-row__image">
					<img src="/assets/public/images/certificate3.png" alt="" class="certificates-row__img d-block">
				</div>
				<div class="certificates-row__image">
					<img src="/assets/public/images/certificate4.png" alt="" class="certificates-row__img d-block">
				</div>
			</div>
		</div>
	</div>
</section>
