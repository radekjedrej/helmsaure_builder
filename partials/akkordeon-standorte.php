<section class="contact-dropdown__wrapper">
	<div class="contact-dropdown wrapper-full">
		<div class="contact-dropdown__container">
			<!-- Tab Start -->
			<div class="contact-dropdown__tab d-tab">
				<div class="contact-dropdown__oblique d-flex d-tab__box">
					<div class="contact-dropdown__box">
						<div class="contact-dropdown__headline">
							<h4 class="contact-dropdown__box__title text-headline f-300">PLZ 0...</h4>
						</div>
						<div class="contact-dropdown__arrow d-tab__arrow">
						<svg class="contact-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="contact-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="contact-dropdown__content">
					<div class="contact-dropdown__flex d-flex d-flex-wrap">
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Bautzen <br>Helmsauer & Kollegen<br> Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wilthener Straße 32</span>
									<span class="contact-dropdown__line contact-dropdown__zip">02625 Bautzen</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 03591/304893</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 03591/304891</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Chemnitz <br>Helmsauer & Preuß GmbH <br></strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Adorfer Straße 17 d</span>
									<span class="contact-dropdown__line contact-dropdown__zip">09123 Chemnitz</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 0911/9292-386</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 0911/9292-101</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Leipzig<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Büttnerstraße 22</span>
									<span class="contact-dropdown__line contact-dropdown__zip">04103 Leipzig</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 0341/4011357</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 0341/4771549</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Tab End -->

			<div class="contact-dropdown__tab d-tab contact-dropdown--image-small">
				<div class="contact-dropdown__oblique d-flex d-tab__box">
					<div class="contact-dropdown__box">
						<div class="contact-dropdown__headline">
							<h4 class="contact-dropdown__box__title text-headline f-300">PLZ 1...</h4>
						</div>
						<div class="contact-dropdown__arrow d-tab__arrow">
						<svg class="contact-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="contact-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="contact-dropdown__content">
					<div class="contact-dropdown__flex d-flex d-flex-wrap">
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Berlin I<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Biesenthaler Straße 6</span>
									<span class="contact-dropdown__line contact-dropdown__zip">13055 Berlin</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 030/28040387</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 030/28044715</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Berlin II<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>	
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Mandelstraße 16</span>
									<span class="contact-dropdown__line contact-dropdown__zip">10409 Berlin</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 030/42022096</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 030/42022097</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Dahme-Spreewald<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>	
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wehnsdorf 03</span>
									<span class="contact-dropdown__line contact-dropdown__zip">15926 Heideblick</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 035455/865780</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 035455/865781</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="contact-dropdown__tab d-tab contact-dropdown--image-small contact-dropdown--revert">
				<div class="contact-dropdown__oblique d-flex d-tab__box">
					<div class="contact-dropdown__box">
						<div class="contact-dropdown__headline">
							<h4 class="contact-dropdown__box__title text-headline f-300">PLZ 2...</h4>
						</div>
						<div class="contact-dropdown__arrow d-tab__arrow">
						<svg class="contact-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="contact-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="contact-dropdown__content">
					<div class="contact-dropdown__flex d-flex d-flex-wrap">
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Bautzen <br>Helmsauer & Kollegen<br> Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wilthener Straße 32</span>
									<span class="contact-dropdown__line contact-dropdown__zip">02625 Bautzen</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 03591/304893</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 03591/304891</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Bautzen <br>Helmsauer & Kollegen<br> Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wilthener Straße 32</span>
									<span class="contact-dropdown__line contact-dropdown__zip">02625 Bautzen</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 03591/304893</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 03591/304891</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Bautzen <br>Helmsauer & Kollegen<br> Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wilthener Straße 32</span>
									<span class="contact-dropdown__line contact-dropdown__zip">02625 Bautzen</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 03591/304893</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 03591/304891</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="contact-dropdown__tab d-tab">
				<div class="contact-dropdown__oblique d-flex d-tab__box">
					<div class="contact-dropdown__box">
						<div class="contact-dropdown__headline">
							<h4 class="contact-dropdown__box__title text-headline f-300">PLZ 3...</h4>
						</div>
						<div class="contact-dropdown__arrow d-tab__arrow">
						<svg class="contact-dropdown__svg" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="contact-dropdown__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="contact-dropdown__content contact-dropdown__segment">
				<div class="contact-dropdown__flex d-flex d-flex-wrap">
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Berlin I<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>		
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Biesenthaler Straße 6</span>
									<span class="contact-dropdown__line contact-dropdown__zip">13055 Berlin</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 030/28040387</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 030/28044715</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Berlin II<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Mandelstraße 16</span>
									<span class="contact-dropdown__line contact-dropdown__zip">10409 Berlin</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 030/42022096</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 030/42022097</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
						<div class="contact-dropdown__column">
							<div class="contact-dropdown__copy text-body f-300">
								<p><strong>Niederlassung Dahme-Spreewald<br>Helmsauer & Kollegen<br>Assekuranzmakler AG</strong></p>
								<p class="contact-dropdown__address">
									<span class="contact-dropdown__line contact-dropdown__street">Wehnsdorf 03</span>
									<span class="contact-dropdown__line contact-dropdown__zip">15926 Heideblick</span>
								</p>
								<p class="contact-dropdown__info">
									<span class="contact-dropdown__line contact-dropdown__tel">Tel. 035455/865780</span>
									<span class="contact-dropdown__line contact-dropdown__fax">Fax 035455/865781</span>
								</p>
								<div class="contact-dropdown__btn">
									<a class="contact-dropdown__link" href="#">eMail</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

