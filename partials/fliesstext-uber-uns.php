<section class="standard-row__wrapper wrapper-full standard-row--revert">
    <div class="standard-row d-flex">
        <div class="standard-row__image">
            <img class="standard-row__img d-block" src="https://via.placeholder.com/430x210" alt="">
        </div>
        <div class="standard-row__content">
            <h4 class="standard-row__title text-header f-300">Über Helmsauer</h4>
            <h5 class="standard-row__subtitle text-subheader f-600">Ein starker Dienstleister stellt sich vor</h5>
            <div class="standard-row__copy text-body f-300">
                <p>Unser Unternehmensverbund im <strong>Portrait</strong>: Ein individuelles und branchenspezifisches <strong>Beratungskonzept</strong> ist die Basis unserer Dienstleistung. Unsere breit gefächerte Kompetenz bauen wir seit 1963 auf – einige Meilensteine liegen somit bereits auf unserem Erfolgsweg. Dank der vielfältigen <strong>Karriere</strong>chancen wachsen wir stetig weiter und gestalten so langfristige Perspektiven. Dabei sind unsere <strong>Standorte</strong> im gesamten DACH-Gebiet verteilt, sodass wir Ihnen den größtmöglichen persönlichen Service bieten können. Neben unserem sozialen <strong>Engagement</strong> ist auch unsere <strong>Management</strong>ebene für uns aktiv: So sind wir zufrieden und können auch unsere Kunden zufrieden stellen.</p>
            </div>
        </div>
    </div>
</section>
