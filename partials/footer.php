<footer class="u-footer">
    <div class="wrapper-full">
        <div class="u-footer__container d-flex">
            <div class="u-footer__left d-flex">
                <div class="u-footer__title text-body f-600 color-white">Zentrale</div>
				<div class="u-footer__address text-links f-300 color-white">Helmsauer Gruppe<br>Dürrenhofstr. 4<br>90402 Nürnberg</div>
				<div class="u-footer__copyright text-copyright color-white f-300">© 2021 HELMSAUER Gruppe</div>
            </div>
            <div class="u-footer__right d-flex">
				<div class="u-footer__top d-flex">
					<div class="u-footer__navigation">
						<ul class="u-footer__list list-reset d-flex">
							<li class="u-footer__item">
								<a href="#" class="u-footer__link text-body f-600 color-white">Karriere</a>
							</li>
							<li class="u-footer__item">
								<a href="#" class="u-footer__link text-body f-600 color-white">Standorte</a>
							</li>
							<li class="u-footer__item">
								<a href="#" class="u-footer__link text-body f-600 color-white">Datenschutz</a>
							</li>
							<li class="u-footer__item">
								<a href="#" class="u-footer__link text-body f-600 color-white">Impressum</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="u-footer__bottom d-flex">
					<div class="u-footer__contact">
						<div class="u-footer__details d-flex">
							<a class="u-footer__piece d-flex" href="mailto:service@helmsauer-gruppe.de">
								<div class="u-footer__svg">
									<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.2 24.1"><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M2.8,1A1.79,1.79,0,0,0,1,2.78V21.3a1.79,1.79,0,0,0,1.78,1.8H21.3a1.79,1.79,0,0,0,1.8-1.78V2.8A1.79,1.79,0,0,0,21.32,1H2.8ZM21.4,24.1H2.8A2.8,2.8,0,0,1,0,21.3V2.8A2.8,2.8,0,0,1,2.8,0H21.3a2.8,2.8,0,0,1,2.8,2.8V21.3a2.66,2.66,0,0,1-2.5,2.8h-.2" transform="translate(0 0)" style="fill:#00adf0"/><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M12.1,20.1a8.15,8.15,0,0,1-5.8-2.5,7.47,7.47,0,0,1-2.2-5.8,8.12,8.12,0,0,1,7.7-7.7A8,8,0,0,1,18,6.7a7.78,7.78,0,0,1,2.1,5.4,6.76,6.76,0,0,1-.4,2.4,2.31,2.31,0,0,1-4.5-.7V12.1h1v1.7a1.38,1.38,0,0,0,2.7.4,7.46,7.46,0,0,0,.3-2.1,7.26,7.26,0,0,0-1.8-4.7,7.13,7.13,0,0,0-5.6-2.3,7,7,0,0,0-6.7,6.7,6.86,6.86,0,0,0,2,5.2,7,7,0,0,0,5.1,2.2v.9Z" transform="translate(0 0)" style="fill:#00adf0"/><rect x="0.1" width="24.1" height="24.1" style="fill:none"/><path d="M11.9,17.1c-2.4,0-4.3-2.2-4.3-5s1.9-5,4.3-5,4.3,2.2,4.3,5h-1c0-2.2-1.5-4-3.3-4s-3.3,1.8-3.3,4,1.5,4,3.3,4a2.89,2.89,0,0,0,2.1-.9l.7.7a4,4,0,0,1-2.8,1.2" transform="translate(0 0)" style="fill:#00adf0"/></svg>
								</div>
								<div class="u-footer__text color-white text-contact f-600">service@helmsauer-gruppe.de</div>
							</a>
							<a class="u-footer__piece d-flex" href="tel:09119292185">
								<div class="u-footer__svg">
									<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.61 24.6"><rect width="24.6" height="24.6" style="fill:none"/><path d="M5,1.8H5m0,0H5m0,0H5m.2,0Zm-.1,0a4,4,0,0,0-4,4h0v.1h0A18,18,0,0,0,18.9,23.6a4,4,0,0,0,4-4,2.74,2.74,0,0,0-.1-.9c0-.1-.1-.2-.1-.3l-6.9-1.5a3.1,3.1,0,0,0-.9,1.8.32.32,0,0,1-.3.3.3.3,0,0,1-.4,0,14.22,14.22,0,0,1-8.6-8.5v-.4c.2-.2.3-.3.4-.3a3.84,3.84,0,0,0,1.8-.9l-1.5-7H6a2.74,2.74,0,0,0-.9-.1h0M18.9,24.6A18.91,18.91,0,0,1,0,5.9V5.8H0A5.12,5.12,0,0,1,5.1.7h0A4.07,4.07,0,0,1,6.2.8a1.94,1.94,0,0,1,.7.2c.2.1.3.2.3.4L8.9,8.9a.74.74,0,0,1-.1.5,3.83,3.83,0,0,1-1.9,1.2,13.37,13.37,0,0,0,7.2,7.2,6.18,6.18,0,0,1,1.2-1.9.43.43,0,0,1,.5-.1l7.4,1.6c.2,0,.3.2.4.3a1.94,1.94,0,0,1,.2.7,4.83,4.83,0,0,1,.1,1.2,5,5,0,0,1-5,5" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M18.3,11.7h-1A4.12,4.12,0,0,0,15.1,8l.5-.9a5.19,5.19,0,0,1,2.7,4.6" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M14.1,7.6a4.86,4.86,0,0,0-1.2-.2v-1a7.52,7.52,0,0,1,1.5.2Z" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M24.6,10.6h-1a9.2,9.2,0,0,0-.5-2.9l1-.3a8.52,8.52,0,0,1,.5,3.2" transform="translate(0 0)" style="fill:#00adf0"/><rect width="24.6" height="24.6" style="fill:none"/><path d="M22.7,6.6A9.45,9.45,0,0,0,14,1V0a10.54,10.54,0,0,1,9.6,6.2Z" transform="translate(0 0)" style="fill:#00adf0"/></svg>
								</div>
								<div class="u-footer__text color-white text-number f-600">0911-9292-185</div>
							</a>
						</div>
					</div>
					<div class="u-footer__socials d-flex">
						<a class="u-footer__socials__svg d-block" href="#">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496.16 347.4"><path d="M485.19,54.4a62.37,62.37,0,0,0-43.9-43.9C402.79,0,247.79,0,247.79,0S92.79.3,54.29,10.8a62.37,62.37,0,0,0-43.9,43.9c-11.6,68.4-16.1,172.6.4,238.3a62.37,62.37,0,0,0,43.9,43.9c38.5,10.5,193.5,10.5,193.5,10.5s155,0,193.5-10.5a62.37,62.37,0,0,0,43.9-43.9C497.79,224.5,501.59,120.3,485.19,54.4ZM198.49,248.2V99.2l128.6,74.5Z" transform="translate(0)" style="fill:#fff"/></svg>
						</a>
						<a class="u-footer__socials__svg d-block" href="#">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492.4 492.4"><path d="M480.4,0H12A12,12,0,0,0,0,12H0V480.4a12,12,0,0,0,12,12H480.4a12,12,0,0,0,12-12V12a12,12,0,0,0-12-12ZM143,328.5H84.4a8.77,8.77,0,0,1-7.7-4,9.29,9.29,0,0,1,0-9L139,205.6a.19.19,0,0,0,0-.27l0,0L99.4,136.7c-1.6-3.3-1.9-6.5-.3-9s4.5-3.7,8-3.7h58.6c9,0,13.4,5.8,16.3,11,0,0,40.1,69.9,40.3,70.3-2.4,4.2-63.3,111.9-63.3,111.9C156,322.6,151.8,328.5,143,328.5ZM420.1,56.6,290.4,286a.3.3,0,0,0,0,.4l82.6,151c1.6,3.2,1.7,6.6.1,9.1s-4.2,3.7-7.8,3.7H306.8c-9,0-13.5-6-16.4-11.2,0,0-83.1-152.4-83.3-152.8,4.2-7.4,130.4-231.3,130.4-231.3,3.1-5.6,6.9-11.2,15.7-11.2h59.2c3.5,0,6.3,1.3,7.8,3.7C421.8,50,421.8,53.3,420.1,56.6Z" transform="translate(0)" style="fill:#fff"/></svg>
						</a>
						<a class="u-footer__socials__svg u-footer__linkedin" href="#">
							<?php include "assets/public/images/svg/linkedin-icon.svg" ?>
						</a>
						<a class="u-footer__socials__svg d-block" href="#">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492.4 492.4"><path d="M480.6,0H11.8A11.82,11.82,0,0,0,0,11.8V480.6a11.82,11.82,0,0,0,11.8,11.8H480.6a11.82,11.82,0,0,0,11.8-11.8V11.8A11.82,11.82,0,0,0,480.6,0ZM322.3,370.9l-1,63.3A10.19,10.19,0,0,1,311,444.3H248.7a10.34,10.34,0,0,1-10.4-10.1V370.9H238V350.5c0-22.1-18.4-40-40.9-40h-22c-22.6,0-40.9,17.9-40.9,40h-.4v83.8a10.19,10.19,0,0,1-10.3,10.1H61.3a10.34,10.34,0,0,1-10.4-10.1V57.4A10.27,10.27,0,0,1,61.3,47.3h62.2a10.34,10.34,0,0,1,10.4,10.1V235.3a128.74,128.74,0,0,1,41.4-6.9h21v-.2c22.6,0,41-17.9,41-39.9l1.3-20.5a10.34,10.34,0,0,1,10.2-8.4H311a10.26,10.26,0,0,1,10.2,8.4h.3v20.4a119.51,119.51,0,0,1-31.3,80.6,120.13,120.13,0,0,1,32.2,81.7v20.4ZM394.4,133,379,148.1a20.53,20.53,0,0,1-28.7.1l-2.5-2.5a19.42,19.42,0,0,1-.44-27.46c.17-.18.36-.36.54-.54l14.4-14.1.9-.9a5.44,5.44,0,0,1,7.5,0l1.9,1.9a5.22,5.22,0,0,1,.22,7.38l0,0-15.4,15a6.4,6.4,0,0,0-.15,9.05l.15.15,2.5,2.5a7,7,0,0,0,9.5.1l15.4-15.1a5.56,5.56,0,0,1,7.6.1l1.9,1.9a5,5,0,0,1,.33,7.06l-.23.24Zm0-61.7-1.9,1.9a5.58,5.58,0,0,1-7.6.2L369.5,58.3a7,7,0,0,0-9.5,0l-2.5,2.5a6.52,6.52,0,0,0-.08,9.22l.08.08,15.4,15a5.12,5.12,0,0,1,.05,7.25l-.15.15-1.9,1.9a5.44,5.44,0,0,1-7.5.1l-15.5-15a19.45,19.45,0,0,1-.7-27.5l.6-.6,2.5-2.5A20.62,20.62,0,0,1,379,49l14.4,14.1.9.9a4.94,4.94,0,0,1,.42,7,3.34,3.34,0,0,1-.32.33Zm54.8,74.3-2.5,2.4a20.53,20.53,0,0,1-28.7-.1l-14.4-14.1-.9-.9a5.13,5.13,0,0,1-.15-7.25l.15-.15,1.9-1.9a5.47,5.47,0,0,1,7.6-.1l15.4,15.1a6.83,6.83,0,0,0,9.4,0l2.5-2.5a6.43,6.43,0,0,0,.21-9.09l-.21-.21-15.4-15a5.14,5.14,0,0,1,0-7.27,1.59,1.59,0,0,1,.13-.13l1.9-1.9a5.44,5.44,0,0,1,7.5-.1L449,117.5a19.55,19.55,0,0,1,.67,27.63Zm0-66.1L434.8,93.6l-1,.9a5.44,5.44,0,0,1-7.5,0l-2-1.9a5.22,5.22,0,0,1-.22-7.38l0,0,15.4-15.1a6.4,6.4,0,0,0,.15-9l-.15-.15L437,58.4a6.85,6.85,0,0,0-9.5,0l-15.4,15a5.56,5.56,0,0,1-7.6-.1l-1.9-1.9a5.1,5.1,0,0,1-.19-7.21l.09-.09L417.9,49a20.62,20.62,0,0,1,28.7-.1l2.5,2.5a19.45,19.45,0,0,1,.7,27.5Z" style="fill:#fff"/></svg>
						</a>
					</div>
					<div class="u-footer__copyright__mobile text-copyright color-white f-300">© 2021 HELMSAUER Gruppe</div>
				</div>
            </div>
        </div>
    </div>
</footer>
