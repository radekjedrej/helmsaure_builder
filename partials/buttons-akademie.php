<section class="buttons-row__wrapper">
	<div class="buttons-row wrapper-full">
		<div class="buttons-row__container d-flex d-flex-wrap">
			<div class="buttons-row__btn">
				<a href="#" class="buttons-row__link u-btn text-subheader">E-Learning</a>
			</div>
			<div class="buttons-row__btn">
				<a href="#" class="buttons-row__link u-btn text-subheader">Seminare</a>
			</div>
			<div class="buttons-row__btn">
				<a href="#" class="buttons-row__link u-btn text-subheader">Webinare</a>
			</div>
			<div class="buttons-row__btn">
				<a href="#" class="buttons-row__link u-btn text-subheader">Workshops</a>
			</div>
		</div>
	</div>
</section>
