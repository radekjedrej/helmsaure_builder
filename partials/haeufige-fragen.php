<section class="dropdown-tabs__wrapper">
	<div class="dropdown-tabs wrapper-small">
		<div class="dropdown-tabs__container">
			<h4 class="dropdown-tabs__title color-lightblue f-300 text-headline">Häufige Fragen</h4>
			<!-- Tab Start -->
			<div class="dropdown-tabs__content d-tab">
				<div class="dropdown-tabs__oblique d-tab__box d-flex">
					<div class="dropdown-tabs__box">
						<div class="dropdown-tabs__headline">
							<h5 class="dropdown-tabs__box__title text-subheader-two f-600">Wie erkenne ich frühzeitig einen Cyberangriff? (ausgeklappt)</h5>
						</div>
						<div class="dropdown-tabs__arrow d-tab__arrow">
						<svg class="dropdown-tabs__svg" width="26" height="41" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="dropdown-tabs__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="dropdown-tabs__text">
						<p class="dropdown-tabs__copy text-body f-300">Dienstleister bieten Phishing-Simulationen an. Sie und ihre Mitarbeitenden erhalten vom Dienstleister über einen bestimmten Zeitraum realitätsnahe Phishing-Emails und können so ihr erlerntes Wissen als praktische Übung trainieren.</p>
					</div>
			</div>
			<!-- Tab End -->
			<div class="dropdown-tabs__content d-tab">
				<div class="dropdown-tabs__oblique d-tab__box d-flex">
					<div class="dropdown-tabs__box">
						<div class="dropdown-tabs__headline">
							<h5 class="dropdown-tabs__box__title text-subheader-two f-600">Wie sieht die steuerliche Handhabe aus? (eingeklappt)</h5>
						</div>
						<div class="dropdown-tabs__arrow d-tab__arrow">
						<svg class="dropdown-tabs__svg" width="26" height="41" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="dropdown-tabs__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="dropdown-tabs__text">
					<p class="dropdown-tabs__copy text-body f-300">Dienstleister bieten Phishing-Simulationen an. Sie und ihre Mitarbeitenden erhalten vom Dienstleister über einen bestimmten Zeitraum realitätsnahe Phishing-Emails und können so ihr erlerntes Wissen als praktische Übung trainieren.</p>
				</div>
			</div>
			<div class="dropdown-tabs__content d-tab">
				<div class="dropdown-tabs__oblique d-tab__box d-flex">
					<div class="dropdown-tabs__box">
						<div class="dropdown-tabs__headline">
							<h5 class="dropdown-tabs__box__title text-subheader-two f-600">Wir öffnen keine E-Mail Anhänge. Dann sind wir doch gar nicht betroffen, oder? (eingeklappt)</h5>
						</div>
						<div class="dropdown-tabs__arrow d-tab__arrow">
						<svg class="dropdown-tabs__svg" width="26" height="41" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="dropdown-tabs__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="dropdown-tabs__text">
					<p class="dropdown-tabs__copy text-body f-300">Dienstleister bieten Phishing-Simulationen an. Sie und ihre Mitarbeitenden erhalten vom Dienstleister über einen bestimmten Zeitraum realitätsnahe Phishing-Emails und können so ihr erlerntes Wissen als praktische Übung trainieren.</p>
				</div>
			</div>
			<div class="dropdown-tabs__content d-tab">
				<div class="dropdown-tabs__oblique d-tab__box d-flex">
					<div class="dropdown-tabs__box">
						<div class="dropdown-tabs__headline">
							<h5 class="dropdown-tabs__box__title text-subheader-two f-600">Verhindern Schulungen nicht den reibungslosen Ablauf in meiner Praxis?</h5>
						</div>
						<div class="dropdown-tabs__arrow d-tab__arrow">
						<svg class="dropdown-tabs__svg" width="26" height="41" viewBox="0 0 26 41" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.7393 20.9296C25.7393 21.9329 25.3286 22.8805 24.5661 23.6051L7.43765 39.8249C5.91251 41.3298 3.39018 41.3298 1.86504 39.8249C0.339908 38.3757 0.339908 35.979 1.86504 34.5298L16.1778 20.9296L1.86504 7.27375C0.339906 5.82456 0.339906 3.42781 1.86504 1.97862C3.39017 0.529423 5.91251 0.529423 7.43764 1.97862L24.5661 18.2542C25.3286 18.9231 25.7393 19.9263 25.7393 20.9296Z" fill="#00ADF0"/></svg>
						</div>
					</div>
					<div class="dropdown-tabs__triangle">
						<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
					</div>
				</div>
				<div class="dropdown-tabs__text">
					<p class="dropdown-tabs__copy text-body f-300">Ihre Mitarbeitenden können die Webinare einzeln staffeln und in mehreren kurzen Etappen absolvieren. Diese sind zu jeder Zeit durchführbar. So lassen sich die kompakten Trainingseinheiten einfach in den Alltag integrieren und behindern die betrieblichen Abläufe nicht. Zudem brauchen Sie keinen Schulungsraum und müssen keine Unterlagen organisieren und bereitstellen.</p>
				</div>
			</div>
		</div>
	</div>
</section>
