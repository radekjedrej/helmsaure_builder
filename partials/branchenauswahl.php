<section class="info-cards__wrapper wrapper-full">
    <div class="info-cards">
        <div class="info-cards__row d-flex d-flex-wrap">
            <!-- Column Start -->
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<info class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Gesundheitswesen Gesundheitswesen Gesundheitswesen Gesundheitswesen</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Ärzte, MVZ, Kliniken, Physio-therapeuten und Leistungs-erbringer  bieten wir indivi-duelle und optimierte Konzepte.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</info>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Ärzte, MVZ, Kliniken, Physio-therapeuten und Leistungs-erbringer  bieten wir indivi-duelle und optimierte Konzepte.</h3>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Abrechnung für Ärzte</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Betriebswirtschaftliche Beratung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">IT-Lösungen</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Services</a>
					</li>
                </ul>
            </div>
            <!-- Column End -->
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Handwerk</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Ob SHK, Schreiner, Glaser oder Metaller, als Handwerksbetrieb erhalten Sie bei uns passgenaue Versicherungskonzepte.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Ob SHK, Schreiner, Glaser oder Metaller, als Handwerksbetrieb erhalten Sie bei uns passgenaue Versicherungskonzepte.</h3>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">IT-Lösungen</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Services</a>
					</li>
                </ul>
            </div>
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Unternehmen</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Für Gewerbe- & Industrie-kunden bieten unsere branchenspezifischen Konzepte eine maßgeschneiderte Lösung.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Für Gewerbe- & Industrie-kunden bieten unsere branchenspezifischen Konzepte eine maßgeschneiderte Lösung.</h3>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">IT-Lösungen</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Services</a>
					</li>
                </ul>
            </div>
            <div class="info-cards__column">
				<a href="#" class="info-cards__segment d-block">
					<div class="info-cards__image">
						<img class="info-cards__img d-block" src="/assets/public/images/Startseite_Branchenauswahl_Gesundheitswesen_AdobeStock_264073464.jpg" alt="">
					</div>
					<div class="info-cards__oblique d-flex">
						<div class="info-cards__box">
							<div class="info-cards__content">
								<h2 class="info-cards__title text-header-three f-300">Freie Berufe und Private</h2>
								<h3 class="info-cards__copy text-body-sm-2 f-600">Freie Berufe wie Steuerberater, Wirtschaftsprüfer und Rechts-anwälte werden mit unseren spezialisierten Konzepten umfassend abgesichert. Zudem bieten wir eine optimale private Absicherung.</h3>
							</div>
						</div>
						<div class="info-cards__triangle">
							<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
						</div>
					</div>
				</a>
                <h3 class="info-cards__copy__mobile text-body-sm-2 f-600">Freie Berufe wie Steuerberater, Wirtschaftsprüfer und Rechts-anwälte werden mit unseren spezialisierten Konzepten umfassend abgesichert. Zudem bieten wir eine optimale private Absicherung.</h3>
                <ul class="info-cards__list d-block list-reset">
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Versicherung</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Bildungsangebot</a>
					</li>
                    <li class="info-cards__item text-body-sm f-300">
						<a href="#" class="info-cards__link">Services</a>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>
