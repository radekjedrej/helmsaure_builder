<section class="heavy-row__wrapper">
	<div class="heavy-row wrapper-full">
		<h4 class="heavy-row__title text-headline f-300">Darauf sollten Sie achten!</h4>
		<div class="heavy-row__container wrapper-small">
			<div class="heavy-row__content d-flex">
				<div class="heavy-row__image">
					<img src="https://via.placeholder.com/800x900" alt="" class="heavy-row__img d-block">
				</div>
				<div class="heavy-row__text">
					<div class="heavy-row__copy text-body f-300">
						<p>Die beste Versicherung ist immer die, die man gar nicht nutzen muss. Das trifft selbstverständlich auch auf die Arzthaftpflichtversicherung zu, denn welcher Arzt möchte gerne einen Fehler begehen? Kommt es allerdings doch zu der Situation, dass Sie in Regress genommen werden könnten, ist es um so wichtiger, dass möglichst alle Risiken versichert sind. Vor allem bei der Arzthaftpflichtversicherung kann dies durchaus Beträge in sechs- oder siebenstelliger Höhe betreffen.</p>

						<p><strong>Wichtig ist, dass die Arzthaftpflicht immer auf einem aktuellen Stand sein sollte, sprich die aktuellen AVB (Allgemeinen Versicherungsbedingungen) beinhaltet und noch wichtiger: Die Arzthaftpflichtversicherung muss stets den aktuellen Gegebenheiten angepasst sein.</strong></p>

						<p>Wir stellen bei unseren Kundinnen und Kunden häufig fest, dass eine bestehende Arzthaftpflichtversicherung eine viel zu geringe Deckungssumme aufweist oder erhebliche Risiken gar nicht abdeckt. Wir ermitteln Ihren individuellen Bedarf und besprechen mit Ihnen die wesentlichen Dinge, die eine Arzthaftpflichtversicherung beinhalten sollte.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
