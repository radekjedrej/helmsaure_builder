<section class="standard-row__wrapper wrapper-full">
    <div class="standard-row d-flex">
        <div class="standard-row__image">
            <img class="standard-row__img d-block" src="https://via.placeholder.com/430x200" alt="">
        </div>
        <div class="standard-row__content">
			<!-- if($title): -->
            <h4 class="standard-row__title text-header f-300">Unser Beratungskonzept</h4>
			<!-- endif; -->

			<!-- if($subtitle): -->
            <h5 class="standard-row__subtitle text-subheader f-600">Branchenspezifische, leistungsstarke Konzepte und Dienstleistungen</h5>
			<!-- endif; -->

            <div class="standard-row__copy text-body f-300">
                <p>Unsere hohe Spezialisierung in verschiedenen Branchen ermöglicht zielgerichtete und passgenaue Lösungen in den Bereichen Versicherung, Finanzdienstleistung, Beratung, Weiterbildung und IT-Service.</p>
                <p>Ihr persönlichen Kundenbetreuer wird vor Ort umfassend für Sie tätig. Ihre individuelle Situation beleuchten wir dabei von allen Seiten. Wir greifen auf jahrzehntelange Erfahrung zurück und fügen diese in den Kontext aktueller Entwicklungen.</p>
            </div>
        </div>
    </div>
</section>
