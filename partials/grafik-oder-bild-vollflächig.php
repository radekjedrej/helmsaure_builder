<section class="image-full__wrapper">
	<div class="image-full wrapper-full">
		<div class="image-full__container d-flex">
			<div class="image-full__image">
				<img src="assets/public/images/big-image.jpg" alt="" class="image-full__img">
			</div>
		</div>
	</div>
</section>
