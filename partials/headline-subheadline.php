<section class="title-row__wrapper">
	<div class="title-row wrapper-full">
		<div class="title-row__container">
			<h2 class="title-row__headline text-header f-300">Unser Leistungsangebot im Detail</h2>
			<h3 class="title-row__subheadline text-subheader f-600">Für Allgemeinmediziner haben wir folgende Versicherungsprodukte optimiert.</h3>
		</div>
	</div>
</section>
