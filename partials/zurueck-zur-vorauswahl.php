<section class="text-and-button__wrapper">
	<div class="text-and-button">
		<div class="text-and-button__container wrapper-full">
			<div class="text-and-button__oblique d-flex">
				<div class="text-and-button__box">
					<h4 class="text-and-button__title text-header f-300">Informieren Sie sich auch über weitere Leistungen für “Facharztauswahl”!</h4>
					<div class="text-and-button__cta d-flex">
						<a href="" class="text-and-button__btn u-btn u-btn--wider text-benefits">Leistungsübersicht</a>
					</div>
				</div>
				<div class="text-and-button__triangle">
					<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.78 984.93"><polyline points="2.39 984.19 307.39 2.5 2.39 2.5" style="fill:none;stroke:#e5f1fa;stroke-miterlimit:10;stroke-width:5px"/></svg>
				</div>
			</div>
		</div>
	</div>
</section>
