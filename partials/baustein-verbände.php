<section class="u-benefits__wrapper">
	<div class="u-benefits wrapper-small">
		<div class="u-benefits__oblique d-flex">
			<div class="u-benefits__box">
				<div class="u-benefits__title text-header f-300">Die Vorteile für Verbandsmitglieder</div>
				<div class="u-benefits__segment d-flex">
					<p class="u-benefits__text text-body color-lightblue f-300">Wir sind Kooperationspartner zahlreicher Verbände. Profitieren auch Sie von den Sonderkonditionen und nutzen Sie den Mehrwert als Verbandsmitglied.</p>
					<div class="u-benefits__cta d-flex">
						<a href="#" class="u-benefits__btn u-btn u-btn--mobile-wider text-number">Mehr Informationen</a>
					</div>
				</div>
			</div>
			<div class="u-benefits__triangle">
				<svg id="Ebene_1" data-name="Ebene 1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 310.69 1000"><polygon points="0 0 0 1000 310.69 0 0 0"/></svg>
			</div>
		</div>

		<!-- List Start -->
		<ul class="u-benefits__list list-reset">
			<!-- List Item Start -->
			<li class="u-benefits__item text-body">Individuelle und branchenspezifische Versicherungslösungen</li>
			<!-- List Item End -->
			<li class="u-benefits__item text-body">Bares Geld sparen über <strong>hohe Rabattierungskompetenzen</strong></li>
			<li class="u-benefits__item text-body"><strong>Persönliche Betreuung</strong> durch feste Kundenberater vor Ort und deren Teampartner im Backoffice</li>
			<li class="u-benefits__item text-body"><strong>Funktionierendes Schadenmanagement</strong>in eigener Abteilung mit Netzwerk für Verbände – aktiv, schnell, zuverlässig!</li>
			<li class="u-benefits__item text-body">Garantierte Bearbeitung innerhalb von 24 Std.</li>
			<li class="u-benefits__item text-body"><strong>Allzeit Transparenz</strong> über unser Tun – Sie sehen was wir sehen! </li>
			<li class="u-benefits__item text-body">myHelmsauer - Onlineportal für Kunden</li>
		</ul>
		<!-- List End -->

		<div class="u-benefits__contact text-benefits color-lightblue f-600">Ihre Verbandshotline für Ärzte: 0911-9292 185</div>
	</div>
</section>
