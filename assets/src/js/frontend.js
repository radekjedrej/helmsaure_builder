/**
 * SASS
 */
import '../sass/frontend.scss'

/**
 * JavaScript
 */

import { Accordion } from './modules/_accordion'
import { LightBox } from './modules/_lightbox'
import { MobileMenu } from './modules/_mobile-menu'
import { Submenu } from './modules/_submenu'
import { SwiperCarousel } from './modules/_swiper'

/**
 * Add here your JavasScript code
 */

window.addEventListener('DOMContentLoaded', () => {
  
  //Variables
  const cardCarousel = document.querySelector('.card-carousel__wrapper')
  const resourcesCarousel = document.querySelector('.resources-carousel__wrapper')
  const dropdownTabs = document.querySelector('.dropdown-tabs__wrapper')
  const lightBoxCards = document.querySelector('.lightbox-cards__wrapper')
  const benefitsDropdown = document.querySelector('.benefits-dropdown__wrapper')
  const standardDropdown = document.querySelector('.standard-dropdown__wrapper');

  //Functions

  new Submenu()
  new MobileMenu()

  if(cardCarousel || resourcesCarousel) {
    new SwiperCarousel()
  }

  if(dropdownTabs || benefitsDropdown || standardDropdown) {
    new Accordion()
  }

  if(lightBoxCards) {
    new LightBox()
  }
})
