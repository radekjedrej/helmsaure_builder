import Swiper, { Navigation, Autoplay, Pagination } from 'swiper'

export class SwiperCarousel {
  constructor() {
    Swiper.use([Navigation, Autoplay, Pagination])

    this.partners
    this.testimonials
    this.init()
  }

  init() {
    this.partnersLogos()
    this.testimonialsCarousel()
  }

  partnersLogos() {
    this.partners = new Swiper('.resources-carousel__carousel', {
      slidesPerView: 1,
      freeMode: true,
      lazy: true,
      loop: true,
      autoplay: {
        delay: 6000,
      },
      spaceBetween: 20,
      navigation: {
        nextEl: '.resources-carousel__right',
        prevEl: '.resources-carousel__left',
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          resistanceRatio: 0,
          spaceBetween: 20,
        },

        1280: {
          slidesPerView: 3,
          resistanceRatio: 0,
          spaceBetween: 20,
        },
      },
    })
  }

  testimonialsCarousel() {
    this.testimonials = new Swiper('.card-carousel__track', {
      slidesPerView: 1,
      freeMode: true,
      lazy: true,
      loop: true,
      autoplay: {
        delay: 6000,
      },
      spaceBetween: 20,
      navigation: {
        nextEl: '.card-carousel__right',
        prevEl: '.card-carousel__left',
      },
      pagination: {
        el: '.card-carousel__pagination',
        clickable: true
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          resistanceRatio: 0,
          spaceBetween: 25,
        },

        1280: {
          slidesPerView: 2,
          resistanceRatio: 0,
          spaceBetween: 40,
        },
      },
    })
  }
}
