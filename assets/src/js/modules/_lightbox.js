import $ from 'jquery'

export class LightBox {
  constructor() {
    this.events();
  }

  events() {
        // Lightbox
        let $overlay = $('<div id="lightbox-overlay"></div>');
        let $imageContainer = $('<div id="lightbox-image"></div>')
        let $image = $("<img>");
        let $exitButton = $('<div id="lightbox-exit" class="text-close f-700 color-white">CLOSE</div>');

        // Add overlay
        $overlay.append($imageContainer).append($exitButton);
        $imageContainer.append($image);
        $(".lightbox-cards__wrapper").append($overlay);

        // Hide overlay on default
        $overlay.hide();

        $(".lightbox-cards__column").click(function(event) {
          event.preventDefault();
          let imageLocation = $(this).find('.lightbox-cards__img').attr("src");
          $image.attr("src", imageLocation);
          $overlay.fadeIn("slow");
        });

        $overlay.click(function() {
          $(this).fadeOut("slow");
        });

        $exitButton.click(function() {
          $("#overlay").fadeOut("slow");
        });
    }
}
  
