import $ from 'jquery'

export class Accordion {
  constructor() {
    this.box = $(".d-tab__box")
    this.events()
  }

  events() {
    this.box.on("click", this.openTab)
  }

  openTab() {
    const text = $(this).siblings()
    
    if(text.length > 0) {
      $(this).toggleClass("d-tab--active")
      text.slideToggle()
    }
  }
}
