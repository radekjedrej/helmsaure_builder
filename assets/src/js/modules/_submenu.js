import $ from 'jquery';

export class Submenu {
	constructor() {
		this.link = $('.l-navbar__link');
		this.dropdownItem = $('.l-navbar__dropdown__item');
		this.submenu = $('.l-navbar__submenu');
		this.item = $('.l-navbar__item');
		this.drop = $('.l-navbar__dropdown');
		this.menu = $('.l-navbar');
		this.events();
	}

	events() {
		this.link.on('click', this.openDropdown);
		this.dropdownItem.on('click', this.openSubmenu);

		$(document).on('click', (e) => {
			if (!this.menu.is(e.target) && this.menu.has(e.target).length === 0) {
				this.drop.removeClass('l-navbar__dropdown--open');
				this.item.removeClass('l-navbar__item--active');
				this.submenu.removeClass('l-navbar__submenu--open');
				this.dropdownItem.removeClass('l-navbar__dropdown__item--active');
			}
		});
	}

	openDropdown(e) {
		const siblings = $(this).parent().parent().siblings();
		const dropdown = $(this).parent().children('.l-navbar__dropdown');
		const container = $(this).parent().parent();
		const menuItem = $('.l-navbar__dropdown__item');
		const subMenu = $('.l-navbar__submenu');

		menuItem.removeClass('l-navbar__dropdown__item--active');
		subMenu.removeClass('l-navbar__submenu--open');

		siblings.each(function(index, el) {
			let drop = el.querySelector('.l-navbar__dropdown');

			if (drop) {
				drop.classList.remove('l-navbar__dropdown--open');
				$(this).removeClass('l-navbar__item--active');
			}
		});

		if (dropdown.length > 0) {
			e.preventDefault();
			container.toggleClass('l-navbar__item--active');
			dropdown.toggleClass('l-navbar__dropdown--open');
		}
	}

	openSubmenu(e) {
		const siblings = $(this).siblings();

		siblings.each(function(index, el) {
			let subMenu = el.querySelector('.l-navbar__submenu');

			if (subMenu) {
				subMenu.classList.remove('l-navbar__submenu--open');
				$(this).removeClass('l-navbar__dropdown__item--active');
			}
		});

		const submenu = $(this).children('.l-navbar__submenu');

		if (submenu.length > 0) {
			if ($(e.target).is('.l-navbar__dropdown__link')) {
				e.preventDefault();
				$(this).toggleClass('l-navbar__dropdown__item--active');
				submenu.toggleClass('l-navbar__submenu--open');
			}
		}
	}
}
