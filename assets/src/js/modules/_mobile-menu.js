import $ from 'jquery'

export class MobileMenu {
  constructor() {
    this.html = $('html')
    this.header = $('.l-navbar')
    this.burger = $('.l-navbar__burger')
    this.menu = $('.m-menu')
    this.link = $('.m-menu__link')
    this.dropdownLink = $('.m-menu__dropdown__link')
    this.submenuLink = $('.m-menu__submenu__link')
    this.drop = $('.m-menu__drop')
    this.allLinks = $('.m-menu__element')

    this.events()
  }

  events() {
    this.burger.on('click', this.openMenu.bind(this))
    this.link.on('click', this.openDropdown)
    this.dropdownLink.on('click', this.openDropdown)
    this.submenuLink.on('click', this.openDropdown)
  }

  openMenu() {
    this.allLinks.removeClass('m-menu__link--active')
    this.drop.slideUp()
    this.html.toggleClass('page-no-scroll')
    this.header.toggleClass('l-navbar--active')
    this.menu.slideToggle()
  }

  openDropdown(e) {
    const drop = $(this).siblings()

    if (drop.length > 0) {
      e.preventDefault()
      $(this).toggleClass('m-menu__link--active')
      drop.slideToggle()
    }
  }
}
