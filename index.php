<?php include('header.php'); ?>

<?php include "partials/navbar.php" ?>

	<main class="main-wrapper">

	<?php include "partials/branchenauswahl.php" ?>

	<?php include "partials/fliesstext-usp.php" ?>

	<?php include "partials/fliesstext-uber-uns.php" ?>

	<?php include "partials/aktuelles.php" ?>

	<?php include "partials/kontaktbaustein.php" ?>

	<?php include "partials/gut-zu-wissen-baustein.php" ?>

	<?php include "partials/abbinder.php" ?>

	<?php include "partials/bereichsauswahl.php" ?>

	<?php include "partials/baustein-verbände.php" ?>

	<?php include "partials/leistungsauswahl.php" ?>

	<?php include "partials/koop-partner.php" ?>

	<?php include "partials/highlights.php" ?>

	<?php include "partials/aktuelles-baustein.php" ?>

	<?php include "partials/empfehlungen-slider.php" ?>

	<?php include "partials/baustein-download.php" ?>

	<?php include "partials/headline-subheadline.php" ?>

	<?php include "partials/fliesstext.php" ?>

	<?php include "partials/beliebteste-produkte.php" ?>

	<?php include "partials/drei-gruende.php" ?>

	<?php include "partials/hinweis.php" ?>

	<?php include "partials/tipp.php" ?>

	<?php include "partials/blaue-headline-und-fliesstext.php" ?>

	<?php include "partials/leistungen-im-detail.php" ?>

	<?php include "partials/wechselservice.php" ?>

	<?php include "partials/zurueck-zur-vorauswahl.php" ?>

	<?php include "partials/haeufige-fragen.php" ?>

	<?php include "partials/stellenanzeigen.php" ?>

	<?php include "partials/bildungsangebote-auswahlboxen.php" ?>

	<?php include "partials/leistungen-im-detail-mit-subheadline.php" ?>

	<?php include "partials/look-and-feel-softwareprodukte.php" ?>

	<?php include "partials/buttons-akademie.php" ?>

	<?php include "partials/bild-text-3-1-umgekehrt.php" ?>

	<?php include "partials/bild-text-3-1.php" ?>

	<?php include "partials/bild-text-2-2-umgekehrt.php" ?>

	<?php include "partials/bild-text-2-2.php" ?>

	<?php include "partials/akkordeon-fliesstext-bild.php" ?>

	<?php include "partials/grafik-oder-bild-vollflächig.php" ?>

	<?php include "partials/grafik-oder-bild-3-4.php" ?>

	<?php include "partials/video-einbindung.php" ?>

	<?php include "partials/akkordeon-standorte.php" ?>

</main>

<?php include "partials/footer.php" ?>

<?php include('footer.php'); ?>
